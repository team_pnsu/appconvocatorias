# PNSU - FRONTEND

## Pasos de configuración e instalación
### <i class="icon-folder-open"></i> Instalación de herramientas

1. Instalar Sublime
2. Instalar ConEmu
1. Instalar Node.js

### <i class="icon-folder-open"></i> Configuramos los archivos a clonar
1. Crear una carpeta del Proyecto ( Ejm: D:/Proyectos/appinclouds )
2. Clonar repositorio. ( git clone https://mfyance@bitbucket.org/mfyance/appinclouds.git )
3. Cambiar el nombre a la carpeta clonada. ( Se bajará con el nombre de appinclouds , cambiar el nombre a frontend , la ruta terminará de la siguiente manera: D:/Proyectos/appinclouds/frontend )

### <i class="icon-folder-open"></i> Ingresamos a la carpeta Frontend

1. Ejecutamos el programa ConEmu.

2. Absolutamente todas la fuentes del desarrollo Frontend deberían agregarse/modificarse en la carpeta frontend.

```
cd frontend/
```
> **Nota:**
> Es recomendable ejecutar el ConEmu a nivel de administrador de Windows.

### <i class="icon-hdd"></i> Instalando las dependencias

1. <i class="icon-cog"></i> Instalar gulp de forma global
```
npm install -g gulp
```

2.  <i class="icon-cog"></i>  Instalar bower de forma global
```
npm install -g bower
```

3.<i class="icon-hdd"></i> Instalando todas las dependencias locales de nuestro proyecto
Una vez situado en la carpeta frontend, debemos instalar todas las dependencias que se encuentran en el archivo <i class="icon-file"></i> **package.json**. 
```
npm install
```
NOTA: En caso de tener el archivo node_modules.zip , ya no será necesario ejecutar esa sentencia, entonces solo copiar y descomprimir el archivo dentro de la carpeta FRONTEND.

### <i class="icon-cog"></i> Ejecutar todas las tareas
Utilizando este comando en la consola ejecutará el archivo <i class="icon-file"></i> **gulpfile.js**. 
```
gulp
```
> **Nota:**
> También se puede ejecutar las [tareas](#todas_las_tareas) en forma independiente

#### <i class="icon-cog"></i> Instalar librerías con bower
```
bower install
```
#### <i class="icon-cog"></i> Ejecutar watchers
```
gulp watch
```
Al ejecutar esta tarea, automáticamente se abrirá una página por defecto, este está configurado en el 

## Tareas Principales

<a name="todas_las_tareas"></a>Tarea | Descripción
---------------- | ---
**gulp clean**   | Tarea para eliminar js, css, fuentes e imagenes previmanete compilados.
**gulp copy**    | Tarea para copiar fuentes e imagenes desde la carpeta base frontend.
**gulp css**     | Tarea para compilar css a partir de archivos stylus.
**gulp html**    | Tarea para compilar html a partir archivos jade.
**gulp js**      | Tarea para compilar javascript a partir de archivos coffee.
**gulp sprites** | Tarea para compilar archivos *.png a una imagen de sprites.
**gulp fonts**   | Tarea para generar css a partir de archivos de fuentes (eot, ttf, woff, svg).
**gulp icons**   | Tarea para generar fuente de iconos a partir de iconos en svg.
**gulp watch**   | Tarea para escuchar cualquier modificación que se en los archivos coffee, stylus, jade y compilarlos automáticamente.


## Herramientas utilizadas
1. [Node js](https://nodejs.org/download/): Entorno Javascript 
2. [ConEMU](https://code.google.com/p/conemu-maximus5/): Terminal
3. [Sublime](http://www.sublimetext.com/3): IDE de desarrollo