yOSON.AppSchema.modules = {
    postulante:{
        controllers:{
            registrar_postulante:{
                actions:{
                    index: function(){
                        yOSON.AppCore.runModule('control_modules_wizard');                        
                        yOSON.AppCore.runModule('wizard');
                        yOSON.AppCore.runModule('load_universities_peru');
                        yOSON.AppCore.runModule('extend_validate');
                        yOSON.AppCore.runModule('validate_datafield_steps');
                        yOSON.AppCore.runModule('ubigeo_peru');
                        yOSON.AppCore.runModule('load_datafields_step_one');
                        yOSON.AppCore.runModule('load_datafields_step_two');
                        yOSON.AppCore.runModule('load_datafields_step_three');
                        yOSON.AppCore.runModule('load_datafields_step_four');
                        yOSON.AppCore.runModule('load_datafields_step_five');
                        yOSON.AppCore.runModule('load_datafields_step_six');
                    },
                    byDefault: function(){}
                },
                allActions: function(){}
            },
            byDefault: function(){}
        },
        allControllers:function(){
        }
    },
    /**
     * @byDefault: De no haber @modules se ejecuta esta por defecto
     */
    byDefault : function(){ /*yOSON.AppCore.runModule('for-all-modules');*/ },

    /**
     * @allModules: Modulos que se ejecutaran en todos los modulos
     * @param {Object} oMCA: Variable JSON con el modulo, controlador y action.
     */
    allModules : function(oMCA){
        yOSON.AppCore.runModule('create_services');
        yOSON.AppCore.runModule('control_process_storage');
        yOSON.AppCore.runModule('control_load_countries');
        yOSON.AppCore.runModule('control_data_store');
        yOSON.AppCore.runModule('control_hash');
        yOSON.AppCore.runModule('control_dt_picker');
    }    
};