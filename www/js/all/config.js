var CONFIG = (function(undefined) {
    "use strict";
    var initialize, fn, ws , url;
    url = {
        vendors:{
            console_log     : PNSU.vendorsPath + "consolelog/consolelog.min.js",
            jquery          : PNSU.vendorsPath + "jquery/dist/jquery.min.js",
            jquery_ui       : PNSU.vendorsPath + "jquery-ui/jquery-ui.min.js",
            easing          : PNSU.vendorsPath + "jquery.easing/js/jquery.easing.min.js",
            bootstrap       : PNSU.vendorsPath + "bootstrap/dist/js/bootstrap.min.js",
            adminlte        : PNSU.vendorsPath + "admin-lte/dist/js/app.min.js",
            material        : PNSU.vendorsPath + "material-design-lite/material.min.js",
            cookies         : PNSU.vendorsPath + "jquery.cookie/jquery.cookie.js",
            moment          : PNSU.vendorsPath + "moment/min/moment.min.js",
            moment_locale   : PNSU.vendorsPath + "moment/min/moment-with-locales.min.js",
            moment_timezone : PNSU.vendorsPath + "moment-timezone/moment-timezone.js"
        },
        libs: {
            yoson           : PNSU.staticsPath + "js/libs/yoson/yoson.js",
            modules         : PNSU.staticsPath + "js/libs/yoson/modules.js",
            load            : PNSU.staticsPath + "js/libs/yoson/load.js"
        },
        css:{
            all             : PNSU.staticsPath + "css/all/"
        },
        js: {
            min             : PNSU.staticsPath + "js/min/",
            main            : PNSU.staticsPath + "js/all/main.js",
            modulos         : PNSU.staticsPath + "js/modulos/"
        }
    };
    fn = {
        environment: function () {
            if(PNSU.appEnviroment === "LOCAL" ){
                fn.estaticosLocales()
            }else{
                fn.estaticosNoLocales()
            }
        },
        estaticosLocales: function () {
            //fn.generateCss(['fonts','main'])
            $LAB
            .script( url.vendors.jquery ).wait(function(){
                $LAB
                .script(url.vendors.jquery_ui)
                .script(url.vendors.easing)
                .script(url.vendors.bootstrap)
                .script(url.vendors.material)
                .script(url.vendors.moment).wait()
                .script(url.vendors.moment_locale)
                .script(url.vendors.moment_timezone)
                .script(url.libs.yoson).wait(function(){
                    $LAB
                    .script(url.js.main).wait(function () {
                        $LAB
                        .script(url.js.modulos + PNSU.module+"/"+PNSU.controller+".js").wait()
                        .script(url.libs.modules).wait(function(){
                            $LAB.script(url.libs.load)
                        })
                    })
                })
            })
        },
        estaticosNoLocales : function () {
            fn.generateCss([
                "fonts.min."    + PNSU.staticsVersion, 
                "main.min."     + PNSU.staticsVersion
            ])

            $LAB
            .script(url.js.min + "vendors.min." + PNSU.staticsVersion+".js").wait(function(){
                $LAB
                .script(url.js.min + "main.min." + PNSU.staticsVersion+".js")
                .script(url.js.min + "pnsu.min." + PNSU.staticsVersion+".js").wait(function () {
                    $LAB
                    .script(url.js.modulos +PNSU.module+"/"+PNSU.controller+".min."+PNSU.staticsVersion+".js").wait()
                    .script(url.js.min + "modules.min."+PNSU.staticsVersion+".js").wait(function(){
                        $LAB.script(url.js.min + "load.min."+PNSU.staticsVersion+".js")
                    })
                })
            })
        },
        consoleLog : function (){
            if( (PNSU.appEnviroment=="LOCAL" || PNSU.appEnviroment=="DES") || (PNSU.appEnviroment=="PRE" && PNSU.debugMode) || (PNSU.appEnviroment=="PRO" && PNSU.debugMode) || PNSU.debugMode ){
                $LAB
                .script(url.vendors.console_log).wait(function () {
                    var script = document.createElement("script");
                    script.innerHTML = "log.settings({lineNumber:false})";
                    document.head.appendChild(script);
                })
            }else{
                window.log = function(){} 
            }
        },
        
        generateCss: function (file) {
            var i, font, style;
            for (i in file){
                font = document.createElement('link')
                font.type = 'text/css'
                font.rel = 'stylesheet'
                font.media = 'all'
                font.href = url.css.all + file[i] + '.csss'
                style = document.getElementsByTagName('head')[document.getElementsByTagName('head').length-1]
                style.appendChild(font, style)
            }
        }
    }

    initialize = function() {
        fn.consoleLog()
        fn.environment()    
    };    
    return {
        init: initialize
    };
})();
CONFIG.init();