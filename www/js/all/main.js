/**
 * Colocar un Hasg en URL
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("control_hash", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
    };
    st = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};    
    suscribeEvents = function () {
        $(window).bind('beforeunload', function(e)  {
            e.preventDefault()            
            return 'Mensaje del Sistema';
        });
        $(window).bind('unload', events.refreshUrl);
    };
    events = {
        refreshUrl: function () {
            window.location.hash = ""
            window.location.href = PNSU.basePath + PNSU.controller + '.html'
        }
    };
    fn = {
        pushHashUrl: function (step) {
            window.location.hash = step
        },
        detectHash: function () {
            var url, hash;
            url = window.location.hash
            hash = url.substring(url.indexOf('#')+1);
            nameHash = {'method':'hash','return':hash}
            return nameHash
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['pushHashUrl'], fn.pushHashUrl , this)
        Sb.events(['detectHash'], fn.detectHash , this)
    };
    return {
        init: initialize
    };
});

/**
 * Crear Session Storage
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("control_process_storage", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws  = {};
    st  = {
        _1 : -1
    };
    beforeCathDom = function(){};
    catchDom = function(){        
    };
    afterCathDom = function(){};
    suscribeEvents = function () {};
    events = {};
    fn = {
        editDataSessionStorage : function (storage, obj) {
            var data,pos,session;
            data    = [] 
            session = sessionStorage.getItem(storage);
            pos = st._1;
            
            if (session !== null && session !== "" && session !== false && session !== void 0) {
                data = JSON.parse(session);
                pos = fn.getPosElement(data, obj.step, obj.name);
            }
            
            if (pos > st._1) {
                data[pos] = obj;
            }else {
                data.push(obj);
            }
            Sb.trigger('createSessionStorage', [storage, data])
        },
        getPosElement: function(data, step, name) {
            var i, result;
            for (i in data) {
                if (data[i].step === step && data[i].name === name) {
                    result = i;
                    break;
                } else {
                    result = st._1;
                }
            }
            return result;
        },
        editDataSessionStorage2 : function (storage, obj) {
            var data,pos,session;
            data    = [] 
            session = sessionStorage.getItem(storage);
            pos = st._1;
            
            if (session !== null && session !== "" && session !== false && session !== void 0) {
                data = JSON.parse(session);
                pos = fn.getPosElement2(data, obj.cod);
            }
            
            if (pos > st._1) {
                data[pos] = obj;
            }else {
                data.push(obj);
            }
            Sb.trigger('createSessionStorage', [storage, data])
        },
        getPosElement2: function(data, cod) {
            var i, result;
            for (i in data) {
                if (data[i].cod === cod) {
                    result = i;
                    break;
                } else {
                    result = st._1;
                }
            }
            return result;
        },
        deleteElementStorage: function (storage, cod) {
            var data, pos, session;
            session = sessionStorage.getItem(storage);
            data = JSON.parse(session);
            pos = fn.getPosElement2(data, cod);
            if (pos > st._1) {
                data.splice(pos, 1);
            }
            fn.createSessionStorage(storage, data)
        },
        createSessionStorage : function (storage, data) {
            var e, exception;
            try {
                sessionStorage.setItem(storage, JSON.stringify(data));
            }  catch (e) {
                exception = e;
            }
        },
        getSessionStorage: function(name){
            session = sessionStorage.getItem(name);
            data = ""
            if (session !== null && session !== "" && session !== false && session !== void 0) {
                data = JSON.parse(session);
            }
            newdata = {'method':'data','return':data}
            return newdata
        },
        getCounterStorage: function(step){
            dataStorage  = Sb.trigger('getSessionStorage', [step])
            
            if(dataStorage.data.length === undefined){
                counter = 0
            }else{
                counter = dataStorage.data.length
            }
            newdata = {'method':'data','return':counter}
            return newdata
        },
        generatorID: function(){
            date = new Date().getTime()
            newdata = {'method':'id','return':date}
            return newdata
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['createSessionStorage'], fn.createSessionStorage, this);
        Sb.events(['getSessionStorage'], fn.getSessionStorage, this);
        Sb.events(['getCounterStorage'], fn.getCounterStorage, this);
        Sb.events(['editDataSessionStorage'], fn.editDataSessionStorage, this);
        Sb.events(['editDataSessionStorage2'], fn.editDataSessionStorage2, this);
        Sb.events(['deleteElementStorage'], fn.deleteElementStorage, this);
        Sb.events(['generatorID'], fn.generatorID, this);
    };
    return {
        init: initialize
    };
});

/**
 * Cerrar sesión del Sistema
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("control_dt_picker", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {
        dtPicker  : '.dt__picker',
        tPicker  : '.t__picker',
        dPicker  : '.d__picker'
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.dtPicker = $(st.dtPicker);
        dom.tPicker  = $(st.tPicker);
        dom.dPicker  = $(st.dPicker);
    };
    afterCathDom = function(){
        dom.dtPicker.datetimepicker({
            locale: 'es'
        });
        dom.tPicker.datetimepicker({
            format: "HH:mm",
            locale: 'es'
        });
        dom.dPicker.datetimepicker({
            format : "YYYY-MM-DD",
            locale: 'es'
        });
    };
    suscribeEvents = function () {};
    events = {};
    fn = {
        loadDateTimePicker : function (element) {
            $(element).datetimepicker({
                locale: 'es'
            });
        },
        loadDatePicker : function (element) {
            $(element).datetimepicker({
                format : "YYYY-MM-DD",
                locale: 'es'
            });
        },
        loadTimePicker : function (element) {
            $(element).datetimepicker({
                format: "HH:mm",
                locale: 'es'        
            });
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['loadDateTimePicker'], fn.loadDateTimePicker , this)
        Sb.events(['loadDatePicker'], fn.loadDatePicker , this)
        Sb.events(['loadTimePicker'], fn.loadTimePicker , this)
    };
    return {
        init: initialize
    };
}, [
    PNSU.vendorsPath +'moment/min/moment-with-locales.min.js',
    PNSU.vendorsPath +'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
]);


/**
 * Crear servicios granulares (cajas de arena)
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("create_services", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};    
    suscribeEvents = function () {};
    events = {};
    fn = {        
        /**
         * Web Service in Ajax Call
         * @param {url}
         * @param {params}
         * @param {method}
         * @returns {ajax}
         */
        configAjax : function( url, params, method ){
            type = (method != undefined) ? method : "POST"
            var ajax = $.ajax({
                type: type,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: url,
                data: params,
                timeout: 30000,
                cache: false,
                beforeSend: function(){
                }
            });
            return ajax;
        },

        /**
         * Create service with Promise Methods
         * @param {url} Service's Url 
         * @param {params} Params to filter consult into service
         * @param {method} Return method ajax
         * @param {success} Return data after ajax call success
         * @param {complete} Return data after ajax call complete
         * @param {error} Return data after ajax call error
         * @returns {void}
                 */
        createServices : function(url, params, method, success, complete, error){
            service = fn.configAjax(url, params, method);
            if ($.isFunction(success) === true) {
                service.done(success);
            }
            if ($.isFunction(complete) === true) {
                service.always(complete);
            }
            if($.isFunction(error) === true) {
                service.fail(error);
            }
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['createServices'], fn.createServices, this);
    };
    return {
        init: initialize
    };
});

/**
 * Extender jQuery Validate - Español
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("extend_validate", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
    };
    st = {
        form        : "form",
        formField   : ".validate__element",
        valError    : "validate__error",
        errorTag    : "em"
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.formField   = $(st.formField)
        dom.form        = $(st.form)
    };
    afterCathDom = function(){
        fn.extendParams()
        fn.addMethods()
        fn.spanishMessages()
    };    
    suscribeEvents = function () {};
    events = {};
    fn = {
        /**
         * Extend ES to Jquery Validate
         * @returns {void}
         */
        extendParams : function () {
            $.validator.setDefaults({
                errorClass: st.errorClass,
                errorElement: 'em',
                errorPlacement: function(error, element) {
                    if (element.is(':radio')) {
                        error.appendTo(element.closest($(st.formField)));
                    } else if (element.is(':checkbox')) {
                        error.appendTo(element.closest($(st.formField)));
                    } else {
                        error.appendTo(element.closest($(st.formField)));
                    }
                },
                showErrors: function(errorMap, errorList) {
                    log('Se presentaros ' + this.numberOfInvalids() + ' errores');
                    this.defaultShowErrors();
                },
                unhighlight: function(element, errorClass, validClass) {
                    var numberOfInvalids;
                    numberOfInvalids = this.numberOfInvalids();
                    if (numberOfInvalids === 0) {
                        dom.form.removeClass('no_errores');                        
                    } else {
                        dom.form.addClass('si_errores');
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).closest($(st.formField)).removeClass('success').addClass('error');
                },
                success: function(element, errorClass, validClass) {
                    $(element).closest($(st.formField)).removeClass('error').addClass('success');
                }
            });
        },
        addMethods: function () {
            $.validator.addMethod("mobile", (function(value, element) {
                return this.optional(element) || /^[9]{1}[0-9]{8}$/gi.test(value);
            }), "");
            $.validator.addMethod("alphNumeric", (function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]+$/gi.test(value);
            }), "");
            $.validator.addMethod("alphabet", (function(value, element) {
                return this.optional(element) || /^[a-zA-ZáéíóúÁÉÍÓÚñÑ& ]+$/gi.test(value);
            }), "");
            $.validator.addMethod("dateMask", (function(value, element) {
                return this.optional(element) || /^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/gi.test(value);
            }), "");
            $.validator.addMethod("nEmail", (function(value, element) {
                return this.optional(element) || /^[a-z0-9\_\-]+(.?[a-z0-9\_\-]+)+@\w+([\.-]?\w+)(\.[a-z]{2,10})$/i.test(value);
            }), "");
            $.validator.addMethod("select", function(value, element, arg){
                return arg != value;
            }, "");
        },
        spanishMessages : function () {
            $.extend($.validator.messages, {
                required: "Este campo es obligatorio.",
                remote: "Por favor, rellena este campo.",
                email: "Por favor, escribe una dirección de correo válida.",
                url: "Por favor, escribe una URL válida.",
                date: "Por favor, escribe una fecha válida.",
                dateISO: "Por favor, escribe una fecha (ISO) válida.",
                number: "Por favor, escribe un número válido.",
                digits: "Por favor, escribe sólo dígitos.",
                creditcard: "Por favor, escribe un número de tarjeta válido.",
                equalTo: "Por favor, escribe el mismo valor de nuevo.",
                extension: "Por favor, escribe un valor con una extensión aceptada.",
                maxlength: $.validator.format("Por favor, no escribas más de {0} caracteres."),
                minlength: $.validator.format("Por favor, no escribas menos de {0} caracteres."),
                rangelength: $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
                range: $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
                max: $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
                min: $.validator.format("Por favor, escribe un valor mayor o igual a {0}."),
                nifES: "Por favor, escribe un NIF válido.",
                nieES: "Por favor, escribe un NIE válido.",
                cifES: "Por favor, escribe un CIF válido.",
                select: "Es necesario elegir una opción"
            });
        },
        addRulesElements: function (element) {
            $.each( element , function (i, el) {
                $(this).rules("add", {
                    required: true,
                    select : 'default'
                }); 
            })
        },
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['addRulesElements'], fn.addRulesElements, this)
    };
    return {
        init: initialize
    };
}, [
    PNSU.vendorsPath + "jquery-validation/dist/jquery.validate.min.js"
]);

/**
 * Controlar la carga de datos guardados en un Storage
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("control_data_store", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};
    suscribeEvents = function () {};
    events = {};
    fn = {
        loadDataStoreSelect : function (storage, element, url, success) {
            complete = function () {
                var name, step, dataStorage, valueStorage, valueLocal;
                name = $(element).attr('name')
                step = $(element).closest("section").data("content")

                dataStorage  = Sb.trigger('getSessionStorage', [storage])
                valStorage = $.grep(dataStorage.data, function (k,i) {
                    return k.name == name
                })                
                if(valStorage.length != 0){
                    valLocal = valStorage[0].value
                    $(element).find('option[value="'+valLocal+'"]').prop("selected", true).trigger("change")
                }
            };
            error = function(e) {
                log('loadDataStoreSelect', e);
            };
            Sb.trigger('createServices' , [url, null, null , success, complete, error])
        },
        loadDataStoreInput : function (storage, element) {
            var name, step, dataStorage, valueStorage, valueLocal;
            $.each( $(element) , function (i,el) {
                name = $(el).attr('name')
                step = $(el).closest("section").data("content")
                dataStorage  = Sb.trigger('getSessionStorage', [storage])
                valStorage = $.grep(dataStorage.data, function (k,i) {
                    return k.name == name
                })
                if(valStorage.length != 0){
                    valLocal = valStorage[0].value
                    $(el).val(valLocal)
                }
            })
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['loadDataStoreSelect'], fn.loadDataStoreSelect, this);
        Sb.events(['loadDataStoreInput'], fn.loadDataStoreInput, this);
    };
    return {
        init: initialize
    };
});

/**
 * Cargar los paises
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("control_load_countries", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
        paises          : PNSU.servicesPath   + "paises" + PNSU.pathJSON
    };
    st = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};
    suscribeEvents = function (){};
    events = {
    };
    fn = {
        cargarPaises: function (storage, element) {
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.name + '">' + k.name + '</option>';
                    $(element).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [storage , $(element) , ws.paises , success])
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['cargarPaises'], fn.cargarPaises , this)
    };
    return {
        init: initialize
    };
});

/**
 * Crear el botón de refrescar grilla DX
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("dx_btn_refresh", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {
        btnRefresh  : '.grid__btn-refresh',
        dxHeader    : '.dx-datagrid-header-panel'
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.btnRefresh  = $(st.btnRefresh);
    };
    afterCathDom = function(){};    
    suscribeEvents = function () {};
    events = {};
    fn = {
        /**
         * Create refresh button into Grid
         * @param {gridContainer} Grid container
         * @returns {void}
         */
        dxBtnRefreshData: function(gridContainer){            
            dom.btnRefresh.dxButton({
                icon: 'fa fa-refresh',
                onClick: function(info) {                    
                    var dataGrid = gridContainer.dxDataGrid('instance');
                    dataGrid.refresh()
                }
            }).prependTo(st.dxHeader);
            setTimeout(function () {
                dom.btnRefresh.attr('title', dom.btnRefresh.data('title'))
            },10)
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['dxBtnRefreshData'], fn.dxBtnRefreshData, this);
    };
    return {
        init: initialize
    };
});
/**
 * Crear el botón de limpiar filtros de grilla DX
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("dx_btn_clear", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {
        btnClear    : '.grid__btn-clear',
        dxHeader    : '.dx-datagrid-header-panel'
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.btnClear  = $(st.btnClear);
    };
    afterCathDom = function(){};    
    suscribeEvents = function () {};
    events = {};
    fn = {
        /**
         * Create clear button into Grid
         * @param {gridContainer} Grid container
         * @returns {void}
         */
        dxBtnClearFilters: function(gridContainer){            
            dom.btnClear.dxButton({
                icon: 'fa fa-eraser',
                onClick: function(info) {
                    var dataGrid = gridContainer.dxDataGrid('instance');
                    dataGrid.clearFilter()
                }
            }).prependTo(st.dxHeader);
            setTimeout(function () {
                dom.btnClear.attr('title', dom.btnClear.data('title'))
            },10)
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['dxBtnClearFilters'], fn.dxBtnClearFilters, this);
    };
    return {
        init: initialize
    };
});
/**
 * Crear un checkbox que ayuda a ocultar/mostrar las cantidades de respuestas de data DX
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("dx_chb_selector", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {
        btnSelector : '.grid__selector'
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.btnSelector = $(st.btnSelector);
    };
    afterCathDom = function(){};    
    suscribeEvents = function () {};
    events = {};
    fn = {
        /**
         * Create checkbox about row sized
         * @param {gridContainer} Grid container
         * @returns {void}
         */
        dxCheckBox: function (gridContainer) {
            dom.btnSelector.dxCheckBox({
                value: false,
                text: 'Mostrar tamaños de filas',
                onValueChanged: function (info) {
                    var dataGrid = gridContainer.dxDataGrid('instance');
                    dataGrid.option({
                        pager: { showPageSizeSelector: info.value }
                    });
                    // dataGrid.repaint();
                }
            }).appendTo(gridContainer);
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['dxCheckBox'], fn.dxCheckBox, this);
    };
    return {
        init: initialize
    };
});
/**
 * Crear paràmetros asìncronos para la grilla DX
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("dx_async_parameters", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    var oCol,oFilter,oFilterDate, dataType,i,j,s,t,colName,colType,colValue,colDataType,gridColumns,operators,tempArr,tempJson,defColData,filters,sorts,oSort;
    dom = {};
    ws = {};
    st = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};    
    suscribeEvents = function () {};
    events = {};
    fn = {
        /**
         * Create PNSU's Async Parameters
         * @param {gridContainer} Grid container
         * @param {filterOptions} Get filterOptions into grid container
         * @param {sortOptions} Get sortOptions into grid container
         * @returns {tempArr} JSON with new parameters
         */
        dxAsyncParameters: function(gridContainer, filterOptions, sortOptions){
            gridColumns = gridContainer.dxDataGrid('instance').option('columns');
            operators   = { contains: 'like' };
            tempArr     = [];
            tempJson    = {};
            defColData  = { col:"", ord:"", value:"", type:"like", datatype:"" };
            filters     = ($.isArray(filterOptions[0])) ? filterOptions : [filterOptions];
            sorts       = ($.isArray(filters[0])) ? sortOptions : [sortOptions];
          
            for (i in gridColumns) {
                oCol         = gridColumns[i];
                colDataType  = oCol.dataType ? oCol.dataType : 'string';
                
                /* 
                 * Recorriendo los Filtros
                 */
                for (var j in filters) {
                    oFilter  = filters[j];
                    oFilterDate = oFilter[0];
                    
                    if ($.isArray(oFilter)) {
                        colName  = oFilter[0];
                        colType  = operators[oFilter[1]];
                        colValue = oFilter[2];
                        /* 
                         * Si item filtro es array y es igual a la columna iterada
                         */
                        if (colValue != undefined && colName === oCol.dataField) {
                            if (colDataType === "date"){
                                var data =  moment(colValue).tz('America/Lima')
                                colValue = moment(data._d).format("DD-MM-YYYY")
                            } 
                            tempJson[colName] = $.extend({}, 
                                tempJson[colName]?tempJson[colName]:defColData, 
                                {col:colName, value:colValue, type:colType, datatype:colDataType} 
                            );
                        }
                    }

                    if ($.isArray(oFilter[0])){
                        colName    = oFilterDate[0];
                        colType    = "like";
                        colValue   = oFilterDate[2];
                        /* 
                         * Si item filtro es array y es igual a la columna iterada
                         */
                        if (colValue != undefined  && colName === oCol.dataField) {
                            if (colDataType === "date"){                                
                                colValue = moment(colValue).format("DD-MM-YYYY")
                            }
                            tempJson[colName] = $.extend( {}, 
                                tempJson[colName] ? tempJson[colName] : defColData,
                                {col:colName, value:colValue, type:colType, datatype:colDataType}
                            );
                        }
                    }
                }
                /* 
                 * Recorriendo los ordenamientos
                 */
                for (s in sorts) {
                    oSort       = sortOptions[s];
                    colName     = oSort.selector;
                    colValue    = oSort.desc ? 'DESC' : 'ASC';
                    
                    if ( colName == oCol.dataField) {
                        tempJson[colName] = $.extend({}, 
                            tempJson[colName]?tempJson[colName]:defColData, 
                            { col:colName , ord:colValue , datatype:colDataType }
                        );
                    }
                }
            }

            for (t in tempJson){ 
                tempArr.push(tempJson[t]);
            }
            return tempArr
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['dxAsyncParameters'], fn.dxAsyncParameters, this);
    };
    return {
        init: initialize
    };
});
/**
 * Extender estas opciones para la grilla DX
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("dx_grid_options", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};    
    suscribeEvents = function () {};
    events = {};
    fn = {
        /**
         * Create Config to Data grid
         * @returns {gridOptions} Config the options to  Data Grid
         */
        dxConfigGridOptions : function () {
            var gridOptions = {               
                selection: { 
                    mode: "none" ,
                    allowSelectAll: true 
                },
                allowColumnReordering: true,
                allowColumnResizing: true,
                hoverStateEnabled: true,
                export: { 
                    fileName: "DataGrid",
                    enabled: true, 
                    allowExportSelectedData: true,
                    excelFilterEnabled: true                
                },
                filterRow: { 
                    visible: true,
                    showOperationChooser: false,
                    applyFilter: "auto"
                }, 
                headerFilter: { 
                    visible: false 
                },
                sorting: { 
                    mode: "multiple" 
                },
                searchPanel: { 
                    visible: false,
                    width: 250 
                },
                paging: {
                    enabled: true,
                    pageSize: 5,
                    pageIndex: 0
                },
                pager: { 
                    visible: true, 
                    showPageSizeSelector: false, 
                    allowedPageSizes: [2, 5, 10],
                    showInfo: true,
                    showNavigationButtons: true
                },
                onRowCollapsed : function () { 
                    log("onRowCollapsed->args:", arguments)  
                },
                onRowCollapsing: function () { 
                    log("onRowCollapsing->args:", arguments) 
                },
                onRowExpanded  : function () { 
                    log("onRowExpanded->args:", arguments)   
                },
                onRowExpanding : function (e) { 
                    log("onRowExpanding->args:", arguments)  
                },
                onSelectionChanged: function (e) {
                    e.component.collapseAll(-1);
                    e.component.expandRow(e.currentSelectedRowKeys[0]);
                }
            }
            return gridOptions
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['dxConfigGridOptions'], fn.dxConfigGridOptions, this);
    };
    return {
        init: initialize
    };
});
/**
 * Extender estas opciones para la gráfica DX
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("dx_chart_options", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};    
    suscribeEvents = function () {};
    events = {};
    fn = {
        /**
         * Create Config to Chart
         * @returns {chartOptions} Config the options to  Chart
         */
        dxConfigChartOptions : function () {
            var chartOptions = {
                palette: 'Soft',
                commonSeriesSettings: { 
                    argumentField: "day", 
                    type: "line" 
                },
                size: { 
                    height: 500 
                },
                legend: {
                    visible: true,
                    border:{
                        visible:true,
                        dashStyle: 'dash'
                    },
                    font: {
                        color: '#222d32',
                        size: 15,
                        family: 'Source Sans Pro'
                    },
                    verticalAlignment: "top",
                    horizontalAlignment: "right"
                },
                commonPaneSettings: { 
                    border: { 
                        visible: true, 
                        width: 1
                    }
                },
            }
            return chartOptions
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['dxConfigChartOptions'], fn.dxConfigChartOptions, this);
    };
    return {
        init: initialize
    };
});
/**
 * Idioma ES para Grilla DX
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("dx_espanish", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){
        fn.globalize()
    };    
    suscribeEvents = function () {};
    events = {};
    fn = {        
        globalize: function () {
            Globalize.addCultureInfo("es-PE", {messages: {
                Yes: "Si",
                No: "No",
                Cancel: "Cancelar",
                Clear: "Limpiar",
                Done: "Listo",
                Loading: "Cargando...",
                Select: "Seleccionando...",
                Search: "Buscar...",
                Back: "Atrás",
                OK: "OK",
                "dxCollectionWidget-noDataText": "No hay datos disponibles",
                "validation-required": "Campo obligatorio",
                "validation-required-formatted": "{0} es un campo obligatorio",
                "validation-numeric": "El valor debe ser un número",
                "validation-numeric-formatted": "{0} debe ser un número",
                "validation-range": "Der Wert ist nicht im gültigen Bereich",
                "validation-range-formatted": "{0} ist nicht im gültigen Bereich",
                "validation-stringLength": "Die Länge des Wertes ist nicht korrekt",
                "validation-stringLength-formatted": "Die Länge von {0} ist nicht korrekt",
                "validation-custom": "Der Wert ist ungültig",
                "validation-custom-formatted": "{0} ist ungültig",
                "validation-compare": "Der Wert ist unpassend",
                "validation-compare-formatted": "{0} ist unpassend",
                "validation-pattern": "Der Wert passt nicht zum Muster",
                "validation-pattern-formatted": "{0} passt nicht zum Muster",
                "validation-email": "Die Email-Adresse ist ungültig",
                "validation-email-formatted": "{0} ist ungültig",
                "validation-mask": "Der Wert ist ungültig"
            }});

            Globalize.addCultureInfo("es-PE", {messages: {
                "dxLookup-searchPlaceholder": "Minimale Anzahl Zeichen: {0}",
                "dxList-pullingDownText": "Zum Aktualisieren nach unten ziehen",
                "dxList-pulledDownText": "Zum Aktualisieren loslassen",
                "dxList-refreshingText": "Aktualisiere...",
                "dxList-pageLoadingText": "Laden...",
                "dxList-nextButtonText": "Mehr",
                "dxList-selectAll": "Todo",
                "dxListEditDecorator-delete": "Entfernen",
                "dxListEditDecorator-more": "Mehr",
                "dxScrollView-pullingDownText": "Zum Aktualisieren nach unten ziehen",
                "dxScrollView-pulledDownText": "Zum Aktualisieren loslassen",
                "dxScrollView-refreshingText": "Aktualisiere...",
                "dxScrollView-reachBottomText": "Laden...",
                "dxDateBox-simulatedDataPickerTitleTime": "Zeit auswählen",
                "dxDateBox-simulatedDataPickerTitleDate": "Datum auswählen",
                "dxDateBox-simulatedDataPickerTitleDateTime": "Datum und Zeit auswählen",
                "dxDateBox-validation-datetime": "Der Wert muss ein Datum oder eine Uhrzeit sein",
                "dxFileUploader-selectFile": "Datei auswählen",
                "dxFileUploader-dropFile": "oder hierher ziehen",
                "dxFileUploader-bytes": "Bytes",
                "dxFileUploader-kb": "kb",
                "dxFileUploader-Mb": "Mb",
                "dxFileUploader-Gb": "Gb",
                "dxFileUploader-upload": "Hochladen",
                "dxFileUploader-uploaded": "Hochgeladen",
                "dxFileUploader-readyToUpload": "Bereit zum hochladen",
                "dxFileUploader-uploadFailedMessage": "Fehler beim hochladen",
                "dxRangeSlider-ariaFrom": "Von {0}",
                "dxRangeSlider-ariaTill": "Bis {0}",
                "dxSwitch-onText": "EIN",
                "dxSwitch-offText": "AUS"
            }});

            Globalize.addCultureInfo("es-PE", {messages: {
                "dxDataGrid-columnChooserTitle": "Spaltenauswahl",
                "dxDataGrid-columnChooserEmptyText": "Ziehen Sie Spalten hierhin, um sie zu verstecken",
                "dxDataGrid-groupContinuesMessage": "Weiter auf der nächsten Seite",
                "dxDataGrid-groupContinuedMessage": "Weiter von der vorherigen Seite",
                "dxDataGrid-editingEditRow": "Bearbeiten",
                "dxDataGrid-editingSaveRowChanges": "Speichern",
                "dxDataGrid-editingCancelRowChanges": "Abbrechen",
                "dxDataGrid-editingDeleteRow": "Entfernen",
                "dxDataGrid-editingUndeleteRow": "Wiederherstellen",
                "dxDataGrid-editingConfirmDeleteMessage": "Sind Sie sicher, dass Sie diesen Datensatz löschen wollen?",
                "dxDataGrid-editingConfirmDeleteTitle": "",
                "dxDataGrid-validationCancelChanges": "!TODO!",
                "dxDataGrid-groupPanelEmptyText": "Ziehen Sie eine Spalte hierhin, um danach zu gruppieren",
                "dxDataGrid-noDataText": "No se encontraron resultados",
                "dxDataGrid-searchPanelPlaceholder": "Búsqueda aquí",
                "dxDataGrid-filterRowShowAllText": "(Todo)",
                "dxDataGrid-filterRowResetOperationText": "Zurücksetzen",
                "dxDataGrid-filterRowOperationEquals": "Ist gleich",
                "dxDataGrid-filterRowOperationNotEquals": "Ist nicht gleich",
                "dxDataGrid-filterRowOperationLess": "Kleiner als",
                "dxDataGrid-filterRowOperationLessOrEquals": "Kleiner oder gleich",
                "dxDataGrid-filterRowOperationGreater": "Größer als",
                "dxDataGrid-filterRowOperationGreaterOrEquals": "Größer oder gleich",
                "dxDataGrid-filterRowOperationStartsWith": "Beginnt mit",
                "dxDataGrid-filterRowOperationContains": "Enthält",
                "dxDataGrid-filterRowOperationNotContains": "Enthält nicht",
                "dxDataGrid-filterRowOperationEndsWith": "Endet mit",
                "dxDataGrid-applyFilterText": "Filter anwenden",
                "dxDataGrid-trueText": "wahr",
                "dxDataGrid-falseText": "falsch",
                "dxDataGrid-sortingAscendingText": "Aufsteigend sortieren",
                "dxDataGrid-sortingDescendingText": "Absteigend sortieren",
                "dxDataGrid-sortingClearText": "Sortierung aufheben",
                "dxDataGrid-editingSaveAllChanges": "Änderungen speichern",
                "dxDataGrid-editingCancelAllChanges": "Änderungen verwerfen",
                "dxDataGrid-editingAddRow": "Neue Zeile",
                "dxDataGrid-summaryMin": "Min: {0}",
                "dxDataGrid-summaryMinOtherColumn": "Minimum von {1} ist {0}",
                "dxDataGrid-summaryMax": "Max: {0}",
                "dxDataGrid-summaryMaxOtherColumn": "Maximum von {1} ist {0}",
                "dxDataGrid-summaryAvg": "Ø: {0}",
                "dxDataGrid-summaryAvgOtherColumn": "Durchschnitt von {1} ist {0}",
                "dxDataGrid-summarySum": "Summe: {0}",
                "dxDataGrid-summarySumOtherColumn": "Summe von {1} ist {0}",
                "dxDataGrid-summaryCount": "Anzahl: {0}",
                "dxDataGrid-columnFixingFix": "Fixieren",
                "dxDataGrid-columnFixingUnfix": "Lösen",
                "dxDataGrid-columnFixingLeftPosition": "Nach links",
                "dxDataGrid-columnFixingRightPosition": "Nach rechts",
                "dxDataGrid-exportTo": "Exportar archivo",
                "dxDataGrid-exportToExcel": "Exportieren als Excel-Datei",
                "dxDataGrid-excelFormat": "Descargar Excel",
                "dxDataGrid-selectedRows": "Filas seleccionadas",
                "dxDataGrid-headerFilterEmptyValue": "(Vacíos)",
                "dxDataGrid-headerFilterOK": "OK",
                "dxDataGrid-headerFilterCancel": "Cancelar",
                "dxDataGrid-ariaColumn": "Columna",
                "dxDataGrid-ariaValue": "Valor",
                "dxDataGrid-ariaFilterCell": "Filterzelle",
                "dxDataGrid-ariaCollapse": "Colapsar",
                "dxDataGrid-ariaExpand": "Expandir",
                "dxDataGrid-ariaDataGrid": "Tabla de datos",
                "dxDataGrid-ariaSearchInGrid": "Buscar en tabla de datos",
                "dxDataGrid-ariaSelectAll": "Seleccionar todo",
                "dxDataGrid-ariaSelectRow": "Seleccionar línea",
                "dxPager-infoText": "Página: {0} / {1}",
                "dxPivotGrid-grandTotal": "Cantidad",
                "dxPivotGrid-total": "{0} Cantidad",
                "dxPivotGrid-fieldChooserTitle": "Feldauswahl",
                "dxPivotGrid-showFieldChooser": "Feldauswahl anzeigen",
                "dxPivotGrid-expandAll": "Alle aufklappen",
                "dxPivotGrid-collapseAll": "Alle zusammenklappen",
                "dxPivotGrid-sortColumnBySummary": "\"{0}\" nach dieser Spalte sortieren",
                "dxPivotGrid-sortRowBySummary": "\"{0}\" nach dieser Zeile sortieren",
                "dxPivotGrid-removeAllSorting": "Sortierungen entfernen",
                "dxPivotGrid-rowFields": "Zeilenfelder",
                "dxPivotGrid-columnFields": "Spaltenfelder",
                "dxPivotGrid-dataFields": "Datenfelder",
                "dxPivotGrid-filterFields": "Filterfelder",
                "dxPivotGrid-allFields": "Alle Felder",
                "dxScheduler-editorLabelTitle": "Betreff",
                "dxScheduler-editorLabelStartDate": "Anfangszeit",
                "dxScheduler-editorLabelEndDate": "Endzeit",
                "dxScheduler-editorLabelDescription": "Beschreibung",
                "dxScheduler-editorLabelRecurrence": "Wiederholung",
                "dxScheduler-openAppointment": "Termin öffnen",
                "dxScheduler-recurrenceNever": "Nie",
                "dxScheduler-recurrenceDaily": "Täglich",
                "dxScheduler-recurrenceWeekly": "Wöchentlich",
                "dxScheduler-recurrenceMonthly": "Monatlich",
                "dxScheduler-recurrenceYearly": "Jährlich",
                "dxScheduler-recurrenceEvery": "Alle",
                "dxScheduler-recurrenceEnd": "Wiederholungsende",
                "dxScheduler-recurrenceAfter": "Nach",
                "dxScheduler-recurrenceOn": "Am",
                "dxScheduler-recurrenceRepeatDaily": "Tag(e)",
                "dxScheduler-recurrenceRepeatWeekly": "Woche(n)",
                "dxScheduler-recurrenceRepeatMonthly": "Monat(e)",
                "dxScheduler-recurrenceRepeatYearly": "Jahr(e)",
                "dxScheduler-switcherDay": "Tag",
                "dxScheduler-switcherWeek": "Woche",
                "dxScheduler-switcherWorkWeek": "Arbeitswoche",
                "dxScheduler-switcherMonth": "Monat",
                "dxScheduler-recurrenceRepeatOnDate": "am Datum",
                "dxScheduler-recurrenceRepeatCount": "Ereignisse",
                "dxScheduler-allDay": "Ganztägig",
                "dxCalendar-todayButtonText": "Heute",
                "dxCalendar-ariaWidgetName": "Kalendar",
                "dxColorView-ariaRed": "Rot",
                "dxColorView-ariaGreen": "Grün",
                "dxColorView-ariaBlue": "Blau",
                "dxColorView-ariaAlpha": "Transparenz",
                "dxColorView-ariaHex": "Farbwert"
            }});
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
}, [
    PNSU.vendorsPath + "globalize/lib/globalize.js",
    PNSU.vendorsPath + "devextreme/js/dx.all.js",
    PNSU.vendorsPath + "globalize/lib/cultures/globalize.culture.es-PE.js"
]);

/**
 * Extender Interacciòn para mostrar exportacion en grilla DX
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("dx_select_export", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {};
    st = {
        html        : 'html',
        chkExportar : '.dx-menu-item-content>.dx-datagrid-checkbox-size',
        gridType    : 'dx-grid'
    };
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};    
    suscribeEvents = function () {
        dom.html.on('click', st.chkExportar, events.controlSelectExport)
    };
    events = {
        /**
         * Functionally to checkbox. True = Show the check to export them
         * @param {_this} Get the context
         * @returns {void}
         */
        controlSelectExport: function(_this){
            var gridContainer = $(_this.currentTarget).closest(dom.html).find("[data-type='"+st.gridType+"']");
            var dataGrid = gridContainer.dxDataGrid('instance');
            setTimeout(function(){
                var status = $(_this.currentTarget).attr('aria-checked');
                dataGrid.option({ selection: { mode: status === "true" ? 'multiple' : 'none' } });
            },400)           
        }
    };
    fn = {};
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
});