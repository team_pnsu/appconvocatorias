/**
 * Colocar un Hasg en URL
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("control_modules_wizard", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
    };
    st = {
        paso1  : "informacion-basica",
        paso2  : "estudios-superiores",
        paso3  : "diplomados",
        paso4  : "experiencia-laboral",
        paso5  : "conocimientos",
        paso6  : "documentacion"
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.step = $(st.step)      
    };
    afterCathDom = function(){
        fn.createSteps()
        fn.pushHashStepOne()
    };    
    suscribeEvents = function () {
        $(window).on('hashchange', events.identifyStep);
    };
    events = {
        identifyStep: function () {            
           step = Sb.trigger('detectHash')
            if(step.hash === st.paso2){
                Sb.trigger("mostrarEstudiosGuardados")                
                return
            }
            if(step.hash === st.paso3){
                Sb.trigger("mostrarCursosGuardados")
                return
            }
            if(step.hash === st.paso4){
                Sb.trigger("mostrarLaboresGuardados")
                Sb.trigger("mostrarTiempoAcumulado")
                return
            }
            if(step.hash === st.paso5){
                Sb.trigger("mostrarConocimientosGuardados")
                return
            }
            if(step.hash === st.paso6){
                Sb.trigger("cargarArchivos")
                return
            }
        }
    };
    fn = {
        pushHashStepOne: function () {
            var tab = Sb.trigger('detectHash')
            if(tab.hash === ""){
                Sb.trigger('pushHashUrl' , [st.paso1])                
                return
            }
        },
        createSteps : function () {
           window.paso1 = st.paso1,
           window.paso2 = st.paso2,
           window.paso3 = st.paso3,
           window.paso4 = st.paso4,
           window.paso5 = st.paso5,
           window.paso6 = st.paso6
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['identifyStep'], events.identifyStep, this)
    };
    return {
        init: initialize
    };
});

/**
 * Cargar Universidades
 * @main www/js/all
 * @author mfyance
 */
yOSON.AppCore.addModule("load_universities_peru", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws  = {
        universities   : PNSU.servicesPath   + "universidades_peru" + PNSU.pathJSON
    };
    st  = {};
    beforeCathDom = function(){};
    catchDom = function(){};
    afterCathDom = function(){};
    suscribeEvents = function () {};
    events = {};
    fn = {
        cargarUniversidades: function (storage, element ) {
            success = function(oResult) {
                var _universidades = new autoComplete({
                    selector: element,
                    minChars: 2,
                    source: function(term, suggest){
                        term = term.toLowerCase();                        
                        var suggestions = [];
                        for (i=0 ; i<oResult.data.length ; i++){
                            if (~oResult.data[i].name.toLowerCase().indexOf(term)) suggestions.push(oResult.data[i].name);
                        }
                        suggest(suggestions);
                    }
                });
            };
            Sb.trigger('loadDataStoreSelect', [storage, element , ws.universities, success])
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['cargarUniversidades'], fn.cargarUniversidades, this);
    };
    return {
        init: initialize
    };
});

/**
 * Registrar postulante
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("wizard", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
    };
    st = {        
        html            : "html,body",
        dtpNacimiento   : "#dtp__nacimiento",
        wizard          : ".wizard",
        processContent  : ".wizard__process",
        processTab      : ".wizard__process--tab",
        btnNext         : ".btn__next",
        btnPrev         : ".btn__prev",
        frmRegistro     : "#form--registro--postulante"
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.html            = $(st.html)
        dom.wizard          = $(st.wizard)
        dom.dtpNacimiento   = $(st.dtpNacimiento)
        dom.processContent  = $(st.processContent)
        dom.processTab      = $(st.processTab)
        dom.btnNext         = $(st.btnNext)
        dom.btnPrev         = $(st.btnPrev)
        dom.frmRegistro     = $(st.frmRegistro)
    };
    afterCathDom = function(){};    
    suscribeEvents = function () {
        dom.processTab.on("click", events.showContentProcess)        
        dom.btnNext.on("click", events.showNextProcess)
        dom.btnPrev.on("click", events.showPrevProcess)
    };
    events = {
        showContentProcess : function () {
            var tab = $(this).data("id")
            if( $(this).hasClass('is--active') || $(this).hasClass('is--complete') ){
                dom.processTab.removeClass('is--active')
                $(this).addClass('is--active')
                dom.processContent.hide()
                Sb.trigger('pushHashUrl' , [tab])
                setTimeout(function () {
                    $(st.processContent + '[data-content="'+tab+'"]').fadeIn('fast')
                },50)
            }
        },
        showPrevProcess : function (e) {
            e.preventDefault()
            var prev = $(this).data("prev")
            fn.triggerShowContentProcess(prev)
            Sb.trigger('pushHashUrl' , [prev])
        },
        showNextProcess: function (e) {
            e.preventDefault()
            var current, next, statusForm;
            current = $(this).data("current")
            next = $(this).data("next")
            statusForm = dom.frmRegistro.valid()
            if(statusForm === true){
                fn.showIconSuccess(current)
                fn.triggerShowContentProcess(next)
                Sb.trigger('pushHashUrl' , [next])
            }else{
                fn.hideIconSuccess(current)
                fn.goToPage()
            }
            return false
        }
    };
    fn = {
        triggerShowContentProcess: function (tab) {
            $(st.processTab + '[data-id="'+tab+'"]').addClass('is--active').trigger('click')
        },
        showIconSuccess: function (tab) {
            $(st.processTab + '[data-id="'+tab+'"]').addClass('is--complete').removeClass('is--active')
        },
        hideIconSuccess: function (tab) {            
            $(st.processTab + '[data-id="'+tab+'"]').removeClass('is--complete')
        },
        goToPage: function () {
            dom.html.animate({
                scrollTop: dom.wizard.offset().top
            },1000);
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
}, [
   PNSU.vendorsPath + "jquery-validation/dist/jquery.validate.min.js"
]);

/**
 * Validacion de los Campos de Registro
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("validate_datafield_steps", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
    };
    st = {
        frmRegistro     : "#form--registro--postulante"       
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.frmRegistro     = $(st.frmRegistro)
    };
    afterCathDom = function(){
      fn.validarCampos()
    };    
    suscribeEvents = function () {};
    events = {};
    fn = {
        validarCampos : function () {
            dom.frmRegistro.validate({
                rules: {
                    // pFechaNacimiento : {
                    //     required: true
                    // },
                    // pPaisNacimiento : {
                    //     required: true,
                    //     select : 'default'
                    // },
                    // pRegNacimiento : {
                    //     required: true,
                    //     select : 'default'
                    // },
                    // regFavoritas : {
                    //     required: true
                    // }
                    // puesto_laboral : {
                    //     select : 'default'
                    // },
                    // diplomado_nombre_estudio: {
                    //     required: true
                    // },
                    // vCorreo:{
                    //     required: true
                    // }
                },
                submitHandler: function (form) {
                    log("submit",form)
                    log("serialize",$(form).serialize())
                    return false
                }
            })
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
}, [
   PNSU.vendorsPath + "jquery-validation/dist/jquery.validate.min.js"
]);

/**
 * Ubigeo Perú
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("ubigeo_peru", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
        ubigeoPeru      : PNSU.servicesPath   + "ubigeo_peru" + PNSU.pathJSON
    };
    st = {
        paisNacimiento  : "select[name='pPaisNacimiento']",
        regNacimiento   : "select[name='pRegNacimiento']",
        provNacimiento  : "select[name='pProvNacimiento']",
        disNacimiento   : "select[name='pDistNacimiento']",
        regResidencia   : "select[name='pRegResidencia']",
        provResidencia  : "select[name='pProvResidencia']",
        disResidencia   : "select[name='pDistResidencia']"
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.paisNacimiento  = $(st.paisNacimiento)
        dom.regNacimiento   = $(st.regNacimiento)
        dom.provNacimiento  = $(st.provNacimiento)
        dom.disNacimiento   = $(st.disNacimiento)
        dom.regResidencia   = $(st.regResidencia)
        dom.provResidencia  = $(st.provResidencia)
        dom.disResidencia   = $(st.disResidencia)
    };
    afterCathDom = function(){
        var step = Sb.trigger('detectHash')
        Sb.trigger('cargarPaises', [step.hash, dom.paisNacimiento])
        fn.cargarRegion(dom.regResidencia)
    };
    suscribeEvents = function () {
        dom.paisNacimiento.on('change', events.detectarPaisNacimiento)
        dom.regNacimiento.on('change', events.detectarRegionNacimiento)
        dom.provNacimiento.on('change', events.detectarProvinciaNacimiento)
        dom.regResidencia.on('change', events.detectarRegionResidencia)
        dom.provResidencia.on('change', events.detectarProvinciaResidencia)
    };
    events = {
        detectarPaisNacimiento: function () {
            pais = $(this).val()
            if( pais === "Peru" ){
                fn.cargarRegion(dom.regNacimiento)
                dom.regNacimiento.prop("disabled", false)
                dom.provNacimiento.prop("disabled", false)
                dom.disNacimiento.prop("disabled", false)
            }else{
                dom.regNacimiento.find('option:gt(0)').remove().end().prop("disabled", true).parent().removeClass('error').children('em').remove()
                dom.provNacimiento.find('option:gt(0)').remove().end().prop("disabled", true).parent().removeClass('error').children('em').remove()
                dom.disNacimiento.find('option:gt(0)').remove().end().prop("disabled", true).parent().removeClass('error').children('em').remove()
            }
        },
        detectarRegionNacimiento: function () {
            dom.provNacimiento.find('option:gt(0)').remove()
            region = $(this).val()
            fn.cargarProvincia(dom.provNacimiento, region)
        },
        detectarProvinciaNacimiento: function () {
            dom.disNacimiento.find('option:gt(0)').remove()
            region = dom.regNacimiento.val()
            provincia = $(this).val()
            fn.cargarDistritos(dom.disNacimiento, region,provincia)
        },
        detectarRegionResidencia: function () {
            dom.provResidencia.find('option:gt(0)').remove()
            region = $(this).val()
            fn.cargarProvincia(dom.provResidencia, region)
        },
        detectarProvinciaResidencia: function () {
            dom.disResidencia.find('option:gt(0)').remove()
            region = dom.regResidencia.val()
            provincia = $(this).val()
            fn.cargarDistritos(dom.disResidencia, region,provincia)
        },
    };
    fn = {        
        cargarRegion: function (element) {
            step = Sb.trigger('detectHash')
            success = function(oResult) {
                var regiones = $.grep(oResult.data, function (k,i) {
                    return (k.provincia === '00' && k.distrito === '00')
                })
                $.each(regiones, function (i,k) {
                    var html = '<option data-ubigeo="'+k.nombre+'" value="' + k.departamento + '">' + k.nombre + '</option>';
                    $(element).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, $(element) , ws.ubigeoPeru , success])
        },
        cargarProvincia: function (element,region) {
            step = Sb.trigger('detectHash')
            success = function(oResult) {
                var provincias = $.grep(oResult.data, function (k,i) {
                    return (k.departamento === region && k.provincia != 0 && k.distrito === '00')
                })
                $.each(provincias, function (i,k) {
                    var html = '<option data-ubigeo="'+k.nombre+'" value="' + k.provincia + '">' + k.nombre + '</option>';
                    $(element).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, $(element) , ws.ubigeoPeru , success])
        },
        cargarDistritos: function (element, region,provincia) {
            step = Sb.trigger('detectHash')
            success = function(oResult) {
                var distritos = $.grep(oResult.data, function (k,i) {
                    return (k.departamento === region && k.provincia === provincia  && k.distrito !== 0)
                })
                $.each(distritos, function (i,k) {
                    var html = '<option data-ubigeo="'+k.nombre+'" value="' + k.distrito + '">' + k.nombre + '</option>';
                    $(element).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, $(element) , ws.ubigeoPeru , success])
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
});

/**
 * PASO 1
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("load_datafields_step_one", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
        dataPrincipal   : PNSU.servicesPath   + "data_principal" + PNSU.pathJSON,
        perfil          : PNSU.servicesPath   + "perfil_laboral" + PNSU.pathJSON,
        regFavoritas    : PNSU.servicesPath   + "regiones" + PNSU.pathJSON,
        ubigeoPeru      : PNSU.servicesPath   + "ubigeo_peru" + PNSU.pathJSON,
        estadoCivil     : PNSU.servicesPath   + "estado_civil" + PNSU.pathJSON,
        tipoTelefono    : PNSU.servicesPath   + "tipo_telefono" + PNSU.pathJSON
    };
    st = {
        step            : "input[name='eTipo'].IB",
        inputs          : "input[type='text'].IB",
        select          : "select.IB",
        codigo          : "input[name='vCodigo']",
        dni             : "input[name='miDNI']",
        ruc             : "input[name='biRUC']",
        nombres         : "input[name='vNombres']",
        apellidos       : "input[name='vApellidos']",
        perfil          : "select[name='xidPerfil']",
        regFavoritas    : "select[name='regFavoritas']",
        estadoCivil     : "select[name='eEstadoCivil']",
        telTipo1        : "select[name='pTipoTel1']",
        telTipo2        : "select[name='pTipoTel2']"
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.step            = $(st.step)
        dom.inputs          = $(st.inputs)
        dom.select          = $(st.select)
        dom.codigo          = $(st.codigo)
        dom.dni             = $(st.dni)
        dom.ruc             = $(st.ruc)
        dom.nombres         = $(st.nombres)
        dom.apellidos       = $(st.apellidos)
        dom.perfil          = $(st.perfil)
        dom.regFavoritas    = $(st.regFavoritas)        
        dom.estadoCivil     = $(st.estadoCivil)
        dom.telTipo1        = $(st.telTipo1)
        dom.telTipo2        = $(st.telTipo2)
    };
    afterCathDom = function(){
        step = Sb.trigger('detectHash')
        if(step === window.paso1)
            Sb.trigger('loadDataStoreInput', [step.hash, dom.inputs])
            fn.cargarDatosPrincipales()
            fn.cargarPerfilLaboral()
            fn.cargarPreferenciaLaboral()
            fn.cargarEstadoCivil()
            fn.cargarTelTipo1()
            fn.cargarTelTipo2()
    };
    suscribeEvents = function () {
        dom.inputs.on( 'focusout', events.elementIsOk)
        dom.select.on( 'change', events.elementIsOk)
    };
    events = {
        elementIsOk : function (e) {
            var name , value, numStep, obj;
            if( $(this).valid() ){                
                name    = $(this).attr('name')
                value   = $(this).val()                
                step    = Sb.trigger('detectHash')
                
                if($.isArray(value))
                    value = value.join()
                
                obj = {
                    eTipo  : dom.step.val(),
                    name   : name,
                    value  : value
                }
                if($(this).find(':selected').data('ubigeo') != null){                    
                    ubigeo   = $(this).find(':selected').data('ubigeo')
                    obj2     = {ubigeo : ubigeo}
                    $.extend( obj, obj2 );
                }               
                Sb.trigger('editDataSessionStorage' , [step.hash, obj])
            }
        }
    };
    fn = {
        cargarDatosPrincipales: function () {            
            success = function (oResult) {
                dom.codigo.val( oResult.data[0].codigo )
                dom.dni.val( oResult.data[0].dni )
                dom.ruc.val( oResult.data[0].ruc )
                dom.nombres.val( oResult.data[0].nombres )
                dom.apellidos.val( oResult.data[0].apellidos )
            };
            error = function(e) {
                log('cargarDatosPrincipales', e);
            };
            Sb.trigger('createServices' , [ws.dataPrincipal, null, null, success, null , error])
        },
        cargarPerfilLaboral: function () {
            step = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    dom.perfil.append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, dom.perfil , ws.perfil , success])
        },
        cargarPreferenciaLaboral: function () {
            success = function (oResult) {
                var regionesSelect2, regiones;
                regionesSelect2 = []
                regiones = $.grep(oResult.data, function (k,i) {
                    return (k.provincia === '00' && k.distrito === '00')
                })
                $.each(regiones, function (i,k) {
                    regionesSelect2.push({
                        id : k.nombre,
                        text : k.nombre
                    })
                })
                dom.regFavoritas.select2({
                    language: "es",
                    data: regionesSelect2,
                    maximumSelectionLength: 3
                });
                // Corregir
                // setTimeout(function () {
                //     dom.regFavoritas.select2().trigger('change');
                // }, 200)
            };
            error = function(e) {
                log('cargarPreferenciaLaboral', e);
            };
            Sb.trigger('createServices' , [ws.ubigeoPeru, null, null , success, null, error])
        },
        cargarEstadoCivil: function () {
            step = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    dom.estadoCivil.append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, dom.estadoCivil , ws.estadoCivil , success])
        },
        cargarTelTipo1: function () {
            step = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    dom.telTipo1.append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, dom.telTipo1 , ws.tipoTelefono , success])
        },
        cargarTelTipo2: function () {
            step = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    dom.telTipo2.append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, dom.telTipo2 , ws.tipoTelefono , success])
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
}, [
    PNSU.vendorsPath + "select2/dist/js/select2.full.min.js",
    PNSU.vendorsPath + "select2/dist/js/i18n/es.js"
]);

/**
 * PASO 2
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("load_datafields_step_two", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
        catEstudio          : PNSU.servicesPath   + "categoria_estudios" + PNSU.pathJSON,
        nivelEstudio        : PNSU.servicesPath   + "nivel_estudios" + PNSU.pathJSON
    };
    st = {
        nivel   : {
            bachiller       : "Bachiller",
            colegiado       : "Colegiado",
            egresado        : "Egresado",
            estudiante      : "Estudiante",
            titulado        : "Titulado"
        },
        frmRegistro         : "#form--registro--postulante",
        formField           : ".validate__element",
        btnAgregar          : ".btn__normal--agregar.btn--is-estudios",
        btnGuardar          : ".btn__normal--guardar.btn--is-estudios",
        btnCancelar         : ".btn__normal--cancelar.btn--is-estudios",
        btnEliminar         : ".btn__eliminar.btn--is-estudios",
        datePicker          : ".d__picker",
        uniAutocomplete     : ".autocomplete--universidad.ESU",
        catEstudio          : "select[name*='pNombreDetalleCV'].ESU",
        nivelEstudio        : "select[name*='pCondicion'].ESU",
        paisEstudio         : "select[name*='pPaisOrg'].ESU",
        numColegiatura      : "input[name*='pCantidad'].ESU",
        fechColegiatura     : "input[name*='pFechaColegiatura'].ESU",
        fechBachiller       : "input[name*='pFechaBachiller'].ESU",
        fechTitulacion      : "input[name*='pFechaTitulacion'].ESU",
        fechIngreso         : "input[name*='pFechaInicio'].ESU",
        fechEgreso          : "input[name*='pFechaFin'].ESU",
        uniEstudio          : "input[name*='pOrganizacion'].ESU",
        nomCarrera          : "input[name*='pDescripcion'].ESU",
        contentEstudios     : "#wizard__estudios--mustache",
        tmpEstudios         : "#tmp__estudios--mustache",
        listaEstudios       : "#wizard__lista_estudios--mustache",
        tmpListaEstudios    : "#tmp__lista_estudios--mustache",
        nuevoEstudio        : ".wizard__lista_estudios--nuevo",
        step                : "input[name='eTipo'].ESU",
        message             : ".message--is-estudios",
        btnNext             : ".btn__next",
        stepClass           : ".ESU"
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.btnAgregar          = $(st.btnAgregar)
        dom.frmRegistro         = $(st.frmRegistro)
        dom.listaEstudios       = $(st.listaEstudios)
        dom.tmpListaEstudios    = $(st.tmpListaEstudios)
        dom.tmpEstudios         = $(st.tmpEstudios)
        dom.contentEstudios     = $(st.contentEstudios)
        dom.step                = $(st.step)
        dom.btnNext             = $(st.btnNext)
    };
    afterCathDom = function(){};
    suscribeEvents = function (){
        dom.btnAgregar.on('click', events.agregarNuevoEstudio)
        dom.frmRegistro.on('click', st.btnGuardar, events.guardarNuevoEstudio)
        dom.frmRegistro.on('click', st.btnCancelar, events.cancelarNuevoEstudio)
        dom.frmRegistro.on( 'click', st.btnEliminar,  events.eliminarNuevoEstudio)
        dom.frmRegistro.on( 'change', st.paisEstudio,  events.obtenerPais)
        dom.frmRegistro.on( 'change', st.nivelEstudio,  events.obtenerNivel)
    };
    events = {
        obtenerNivel: function () {
            var nivel = $(this).val()
            switch(nivel){
                case st.nivel.estudiante :
                    $(st.numColegiatura).parent().fadeOut()
                    $(st.fechEgreso).parent().parent().fadeOut()
                    $(st.fechBachiller).parent().parent().fadeOut()
                    $(st.fechTitulacion).parent().parent().fadeOut()
                    $(st.fechColegiatura).parent().parent().fadeOut()
                    break
                case st.nivel.bachiller :
                    $(st.numColegiatura).parent().fadeOut()
                    $(st.fechEgreso).parent().parent().fadeIn()
                    $(st.fechBachiller).parent().parent().fadeIn()
                    $(st.fechTitulacion).parent().parent().fadeOut()
                    $(st.fechColegiatura).parent().parent().fadeOut()
                    break
                case st.nivel.egresado :
                    $(st.numColegiatura).parent().fadeOut()
                    $(st.fechEgreso).parent().parent().fadeIn()
                    $(st.fechBachiller).parent().parent().fadeOut()
                    $(st.fechTitulacion).parent().parent().fadeOut()
                    $(st.fechColegiatura).parent().parent().fadeOut()
                    break
                case st.nivel.titulado :
                    $(st.numColegiatura).parent().fadeOut()
                    $(st.fechEgreso).parent().parent().fadeIn()
                    $(st.fechBachiller).parent().parent().fadeIn()
                    $(st.fechTitulacion).parent().parent().fadeIn()
                    $(st.fechColegiatura).parent().parent().fadeOut()
                    break
                case st.nivel.colegiado :
                    $(st.numColegiatura).parent().fadeIn()
                    $(st.fechEgreso).parent().parent().fadeIn()
                    $(st.fechBachiller).parent().parent().fadeIn()
                    $(st.fechTitulacion).parent().parent().fadeIn()
                    $(st.fechColegiatura).parent().parent().fadeIn()
                    break                                  
            }
        },
        obtenerPais: function () {
            step  = Sb.trigger('detectHash')
            if($(this).val() === "Peru"){
                $(st.uniEstudio).parent().fadeIn()
                Sb.trigger('cargarUniversidades', [ step.hash, st.uniAutocomplete])
            }else{
                $(st.uniEstudio).val('')
                $(st.uniEstudio).parent().fadeOut()
            }
        },
        agregarNuevoEstudio: function () {
            dom.btnAgregar.hide()
            if(dom.frmRegistro.valid() === true){
                var step, counter, data;
                step = Sb.trigger('detectHash')
                cod = Sb.trigger('generatorID')
                data = {
                    eTipo           : dom.step.val(),
                    id              : cod.id
                }
                template = dom.tmpEstudios.html()
                info     =  Mustache.render(template, data)
                dom.contentEstudios.html(info)

                Sb.trigger('loadDatePicker', [$(st.datePicker)])
                Sb.trigger('cargarPaises', [step.hash, $(st.paisEstudio)])
                fn.cargarCategoriaEstudios()
                fn.cargarNivelEstudios()
                Sb.trigger('addRulesElements', [ $(st.stepClass) ])
            }else{
                // alert("No PASA")
            }
        },
        cancelarNuevoEstudio: function () {
            dom.btnAgregar.show()
            dom.frmRegistro.data('validator').resetForm();
            dom.frmRegistro.find($(st.formField)).removeClass('error')
            dom.contentEstudios.empty()
        },        
        guardarNuevoEstudio: function () {
            if(dom.frmRegistro.valid() === true){
                var step, counter, data;
                step = Sb.trigger('detectHash')
                cod = Sb.trigger('generatorID')

                data = {
                    eTipo             : dom.step.val(),
                    id                : cod.id,
                    cod               : dom.step.val()+'-'+cod.id,
                    pNombreDetalleCV  : $(st.catEstudio).val(),
                    pCondicion        : $(st.nivelEstudio).val(),
                    pCantidad         : $(st.numColegiatura).val(),
                    pDescripcion      : $(st.nomCarrera).val(),
                    pPaisOrg          : $(st.paisEstudio).val(),
                    pOrganizacion     : $(st.uniEstudio).val(),
                    pFechaInicio      : $(st.fechIngreso).val(),
                    pFechaFin         : $(st.fechEgreso).val(),
                    pFechaBachiller   : $(st.fechBachiller).val(),
                    pFechaTitulacion  : $(st.fechTitulacion).val(),
                    pFechaColegiatura : $(st.fechColegiatura).val()
                }                
                Sb.trigger('editDataSessionStorage2' , [step.hash , data])
                fn.mostrarEstudiosGuardados()
                dom.contentEstudios.empty()
                dom.btnAgregar.show()
            }else{
            }
        },
        eliminarNuevoEstudio: function () {
            step      = Sb.trigger('detectHash')
            cod = $(this).data('cod')
            Sb.trigger('deleteElementStorage', [step.hash, cod])
            fn.mostrarEstudiosGuardados()
        }
    };
    fn = {
        mostrarEstudiosGuardados: function () {
            dom.listaEstudios.empty()
            step      = Sb.trigger('detectHash')
            data      = Sb.trigger('getSessionStorage', [step.hash])
            template  = dom.tmpListaEstudios.html()
            info      =  Mustache.render(template, data)
            dom.listaEstudios.append(info)

            if($(st.nuevoEstudio).length > 0)
                $(st.message).hide().closest('article').find(dom.btnNext).prop("disabled", false)
            else
                $(st.message).show().closest('article').find(dom.btnNext).prop("disabled", true)
        },
        cargarCategoriaEstudios : function () {
            step      = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    $(st.catEstudio).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, $(st.catEstudio) , ws.catEstudio , success])
        },
        cargarNivelEstudios : function () {
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    $(st.nivelEstudio).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, $(st.nivelEstudio) , ws.nivelEstudio , success])
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['mostrarEstudiosGuardados'], fn.mostrarEstudiosGuardados, this)
    };
    return {
        init: initialize
    };
},[
    PNSU.vendorsPath + "mustache.js/mustache.min.js",
    PNSU.vendorsPath + "javascript-auto-complete/auto-complete.min.js"
]);


/**
 * PASO 3
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("load_datafields_step_three", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
        catCurso            : PNSU.servicesPath   + "otros_estudios" + PNSU.pathJSON
    };
    st = {
        frmRegistro         : "#form--registro--postulante",
        formField           : ".validate__element",
        btnAgregar          : ".btn__normal--agregar.btn--is-cursos",
        btnGuardar          : ".btn__normal--guardar.btn--is-cursos",
        btnCancelar         : ".btn__normal--cancelar.btn--is-cursos",
        btnEliminar         : ".btn__eliminar.btn--is-cursos",
        datePicker          : ".d__picker.CUR",
        uniAutocomplete     : ".autocomplete--universidad.CUR",
        catCurso            : "select[name^='pNombreDetalleCV'].CUR",
        paisCurso           : "select[name^='pPaisOrg'].CUR",
        nomCurso            : "input[name^='pDescripcion'].CUR",
        uniCurso            : "input[name^='pOrganizacion'].CUR",
        fechIngreso         : "input[name^='pFechaInicio'].CUR",
        fechEgreso          : "input[name^='pFechaFin'].CUR",
        contentCursos       : "#wizard__cursos--mustache",
        tmpCursos           : "#tmp__cursos--mustache",
        listaCursos         : "#wizard__lista_cursos--mustache",
        tmpListaCursos      : "#tmp__lista_cursos--mustache",
        nuevoCurso          : ".wizard__lista_cursos--nuevo",
        step                : "input[name='eTipo'].CUR",
        message             : ".message--is-cursos",
        btnNext             : ".btn__next",
        stepClass           : ".CUR"
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.btnAgregar          = $(st.btnAgregar)
        dom.frmRegistro         = $(st.frmRegistro)
        dom.contentCursos       = $(st.contentCursos)
        dom.listaCursos         = $(st.listaCursos)
        dom.tmpCursos           = $(st.tmpCursos)
        dom.tmpListaCursos      = $(st.tmpListaCursos)
        dom.step                = $(st.step)
        dom.btnNext             = $(st.btnNext)
    };
    afterCathDom = function(){};
    suscribeEvents = function (){
        dom.btnAgregar.on('click', events.agregarNuevoCurso)
        dom.frmRegistro.on('click', st.btnGuardar, events.guardarNuevoCurso)
        dom.frmRegistro.on('click', st.btnCancelar, events.cancelarNuevoCurso)
        dom.frmRegistro.on('click', st.btnEliminar, events.eliminarNuevoCurso)
        dom.frmRegistro.on( 'change', st.paisCurso,  events.obtenerPais)
    };
    events = {
        obtenerPais: function (event) {
            step  = Sb.trigger('detectHash')
            if($(this).val() === "Peru"){
                $(st.uniCurso).parent().fadeIn()
                Sb.trigger('cargarUniversidades', [ step.hash, st.uniAutocomplete])
            }else{
                $(st.uniCurso).val('')
                $(st.uniCurso).parent().fadeOut()
            }
        },
        agregarNuevoCurso: function () {
            dom.btnAgregar.hide()
            if(dom.frmRegistro.valid() === true){
                var step, counter, data;
                step = Sb.trigger('detectHash')
                cod = Sb.trigger('generatorID')
                data = {
                    eTipo           : dom.step.val(),
                    id              : cod.id
                }
                template = dom.tmpCursos.html()
                info     =  Mustache.render(template, data)
                dom.contentCursos.html(info)
                
                Sb.trigger('loadDatePicker', [$(st.datePicker)])
                Sb.trigger('cargarPaises', [step.hash, $(st.paisCurso)])
                fn.cargarCategoriasCursos()
                Sb.trigger('addRulesElements', [ $(st.stepClass) ])
            }else{
                // alert("No PASA")
            }
        },       
        cancelarNuevoCurso: function () {
            dom.btnAgregar.show()
            dom.frmRegistro.data('validator').resetForm();
            dom.frmRegistro.find($(st.formField)).removeClass('error')
            dom.contentCursos.empty()
        },
        guardarNuevoCurso: function () {
            if(dom.frmRegistro.valid() === true){
                var step, counter, data;
                step = Sb.trigger('detectHash')
                cod = Sb.trigger('generatorID')

                data = {
                    eTipo             : dom.step.val(),
                    id                : cod.id,
                    cod               : dom.step.val()+'-'+cod.id,
                    pNombreDetalleCV  : $(st.catCurso).val(),
                    pPaisOrg          : $(st.paisCurso).val(),
                    pDescripcion      : $(st.nomCurso).val(),
                    pFechaInicio      : $(st.fechIngreso).val(),
                    pFechaFin         : $(st.fechEgreso).val(),
                    pOrganizacion     : $(st.uniCurso).val()
                }
                Sb.trigger('editDataSessionStorage2' , [step.hash , data])
                fn.mostrarCursosGuardados()
                dom.contentCursos.empty()
                dom.btnAgregar.show()
            }else{
            }
        },
        eliminarNuevoCurso: function () {
            step      = Sb.trigger('detectHash')
            cod = $(this).data('cod')
            Sb.trigger('deleteElementStorage', [step.hash, cod])
            fn.mostrarCursosGuardados()
        }
    };
    fn = {
        mostrarCursosGuardados: function () {
            dom.listaCursos.empty()
            step      = Sb.trigger('detectHash')
            data      = Sb.trigger('getSessionStorage', [step.hash])
            template  = dom.tmpListaCursos.html()
            info      =  Mustache.render(template, data)
            dom.listaCursos.append(info)

            if($(st.nuevoCurso).length > 0)
                $(st.message).hide().closest('article').find(dom.btnNext).prop("disabled", false)
            else
                $(st.message).show().closest('article').find(dom.btnNext).prop("disabled", true)
        },
        cargarCategoriasCursos : function () {
            step      = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    $(st.catCurso).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash, $(st.catCurso) , ws.catCurso , success])
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['mostrarCursosGuardados'], fn.mostrarCursosGuardados, this)
    };
    return {
        init: initialize
    };
},[
    PNSU.vendorsPath + "mustache.js/mustache.min.js",
    PNSU.vendorsPath + "javascript-auto-complete/auto-complete.min.js"
]);

/**
 * PASO 4
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("load_datafields_step_four", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
        catLaboral          : PNSU.servicesPath   + "categoria_laboral" + PNSU.pathJSON,
        secLaboral          : PNSU.servicesPath   + "sector_laboral" + PNSU.pathJSON
    };    
    st = {
        frmRegistro         : "#form--registro--postulante",
        formField           : ".validate__element",
        btnAgregar          : ".btn__normal--agregar.btn--is-laboral",
        btnGuardar          : ".btn__normal--guardar.btn--is-laboral",
        btnCancelar         : ".btn__normal--cancelar.btn--is-laboral",
        btnEliminar         : ".btn__eliminar.btn--is-laboral",
        datePicker          : ".d__picker",
        inputs              : "input[type='text'].ELE",
        catLaboral          : "select[name*='pNombreDetalleCV'].ELE",
        paisLaboral         : "select[name*='pPaisOrg'].ELE",
        secLaboral          : "select[name*='pCondicion'].ELE",
        lugarLaboral        : "input[name*='pOrganizacion'].ELE",
        puestoLaboral       : "input[name*='pDescripcion'].ELE",
        fechIngreso         : "input[name*='pFechaInicio'].ELE",
        fechEgreso          : "input[name*='pFechaFin'].ELE",
        contentLaboral      : "#wizard__laboral--mustache",
        tmpLaboral          : "#tmp__laboral--mustache",
        listaLaboral        : "#wizard__lista_laboral--mustache",
        tmpListaLaboral     : "#tmp__lista_laboral--mustache",
        nuevoLaboral        : ".wizard__lista_laboral--nuevo",
        chkActual           : "#pTrabajoActual",
        step                : "input[name='eTipo'].ELE",
        tiempoTotal         : ".wizard__tiempo__acumulado",
        mainTiempoTotal     : ".wizard__tiempo",
        message             : ".message--is-laboral",
        btnNext             : ".btn__next",
        stepClass           : ".ELE",
        time : {
            year: "año",
            years: "años",
            month: "mes",
            months: "meses",
            day: "dia",
            days: "dias"
        }
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.btnAgregar          = $(st.btnAgregar)
        dom.frmRegistro         = $(st.frmRegistro)
        dom.contentLaboral      = $(st.contentLaboral)
        dom.listaLaboral        = $(st.listaLaboral)
        dom.tmpLaboral          = $(st.tmpLaboral)
        dom.tmpListaLaboral     = $(st.tmpListaLaboral)
        dom.step                = $(st.step)
        dom.btnNext             = $(st.btnNext)
        dom.tiempoTotal         = $(st.tiempoTotal)
        dom.mainTiempoTotal     = $(st.mainTiempoTotal)
    };
    afterCathDom = function(){};
    suscribeEvents = function (){
        dom.btnAgregar.on('click', events.agregarNuevoLaboral)
        dom.frmRegistro.on('click', st.btnGuardar, events.guardarNuevoLaboral)
        dom.frmRegistro.on('click', st.btnCancelar, events.cancelarNuevoLaboral)
        dom.frmRegistro.on('click', st.btnEliminar, events.eliminarNuevoLaboral)
        dom.frmRegistro.on('click', st.chkActual, events.fechaTerminoLaboral)
    };
    events = {
        fechaTerminoLaboral: function () {
            $(this).is(":checked") ? $(st.fechEgreso).parent().parent().fadeOut() : $(st.fechEgreso).parent().parent().fadeIn()           
        },
        agregarNuevoLaboral: function () {
            dom.btnAgregar.hide()
            if(dom.frmRegistro.valid() === true){
                var step, counter, data;
                step = Sb.trigger('detectHash')
                cod = Sb.trigger('generatorID')
                data = {
                    eTipo           : dom.step.val(),
                    id              : cod.id,
                    fecha           : moment().format("YYYY-MM-DD")
                }
                template = dom.tmpLaboral.html()
                info     =  Mustache.render(template, data)
                dom.contentLaboral.html(info)
                
                Sb.trigger('loadDatePicker', [$(st.datePicker)])
                Sb.trigger('cargarPaises', [step.hash, $(st.paisLaboral)])
                fn.cargarTiposExperiencia()
                fn.cargarSectores()
                Sb.trigger('addRulesElements', [ $(st.stepClass) ])
            }else{
                // alert("No PASA")
            }
        },
        cancelarNuevoLaboral: function () {
            dom.btnAgregar.show()
            dom.frmRegistro.data('validator').resetForm();
            dom.frmRegistro.find($(st.formField)).removeClass('error')
            dom.contentLaboral.empty()
        },
        guardarNuevoLaboral: function () {
            if(dom.frmRegistro.valid() === true){
                var step, counter, data;
                step = Sb.trigger('detectHash')
                cod = Sb.trigger('generatorID')

                data = {
                    eTipo             : dom.step.val(),
                    id                : cod.id,
                    cod               : dom.step.val()+'-'+cod.id,
                    pNombreDetalleCV  : $(st.catLaboral).val(),
                    pPaisOrg          : $(st.paisLaboral).val(),
                    pCondicion        : $(st.secLaboral).val(),
                    pOrganizacion     : $(st.lugarLaboral).val(),
                    pDescripcion      : $(st.puestoLaboral).val(),
                    pFechaInicio      : $(st.fechIngreso).val(),
                    pFechaFin         : $(st.fechEgreso).val()
                }

                if($(st.chkActual).is(':checked')){
                    fin  = $(st.chkActual).val()
                    obj  = {pFechaFin : fin}
                    $.extend( data, obj );
                }    

                Sb.trigger('editDataSessionStorage2' , [step.hash , data])
                fn.mostrarLaboresGuardados()
                fn.mostrarTiempoAcumulado()
                dom.contentLaboral.empty()
                dom.btnAgregar.show()
            }else{
            }
        },
        eliminarNuevoLaboral: function () {
            step = Sb.trigger('detectHash')
            cod  = $(this).data('cod')
            Sb.trigger('deleteElementStorage', [step.hash, cod])
            fn.mostrarLaboresGuardados()
            fn.mostrarTiempoAcumulado()
        }
    };
    fn = {
        mostrarTiempoAcumulado: function () {
            step      = Sb.trigger('detectHash')
            data      = Sb.trigger('getSessionStorage', [step.hash])
            total = 0
            $.each(data.data, function (i,k) {
                start = moment(k.pFechaFin);
                end = moment(k.pFechaInicio);
                subtotal = start.diff(end, "days")
                total = total + parseInt(subtotal)
            });

            moment1 = new moment();
            moment2 = new moment(moment().add(total, 'days'))
            diffInMilliSeconds = moment2.diff(moment1);
            duration = moment.duration(diffInMilliSeconds);

            years = duration.years();
            months = duration.months();
            days = duration.days();
            
            if(years === 0)
                txtYear = ""
            else if(years === 1)
                txtYear = years + " " + st.time.year
            else
                txtYear = years + " " + st.time.years

            if(months === 0)
                txtMonth = ""
            else if(months === 1)
                txtMonth = months + " " + st.time.month
            else
                txtMonth = months + " " + st.time.months

            if(days === 0)
                txtDay = ""
            else if(days === 1)
                txtDay = days + " " + st.time.day
            else
                txtDay = days + " " + st.time.days

            dom.tiempoTotal.closest(dom.mainTiempoTotal).show().end().text(txtYear +" "+ txtMonth +" "+ txtDay)

            if(years === 0 && months === 0 && days === 0)
                dom.tiempoTotal.closest(dom.mainTiempoTotal).hide()
        },
        mostrarLaboresGuardados: function () {
            dom.listaLaboral.empty()
            step      = Sb.trigger('detectHash')
            data      = Sb.trigger('getSessionStorage', [step.hash])
            template  = dom.tmpListaLaboral.html()
            info      =  Mustache.render(template, data)
            dom.listaLaboral.append(info)
            
            if($(st.nuevoLaboral).length > 0)
                $(st.message).hide().closest('article').find(dom.btnNext).prop("disabled", false)
            else
                $(st.message).show().closest('article').find(dom.btnNext).prop("disabled", true)
        },
        cargarTiposExperiencia : function () {
            step      = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    $(st.catLaboral).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash , $(st.catLaboral) , ws.catLaboral , success])
        },
        cargarSectores : function () {
            step      = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    $(st.secLaboral).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash , $(st.secLaboral) , ws.secLaboral , success])
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['mostrarLaboresGuardados'], fn.mostrarLaboresGuardados, this)
        Sb.events(['mostrarTiempoAcumulado'], fn.mostrarTiempoAcumulado, this)
    };
    return {
        init: initialize
    };
},[
    PNSU.vendorsPath + "mustache.js/mustache.min.js",
    PNSU.vendorsPath + "javascript-auto-complete/auto-complete.min.js"
]);

/**
 * PASO 5
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("load_datafields_step_five", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
        areaConocimiento     : PNSU.servicesPath   + "areas_conocimientos" + PNSU.pathJSON,
        nomConocimiento      : PNSU.servicesPath   + "conocimientos" + PNSU.pathJSON,
        nivelConocimiento    : PNSU.servicesPath   + "nivel_conocimientos" + PNSU.pathJSON
    };
    st = {
        frmRegistro           : "#form--registro--postulante",
        formField             : ".validate__element",
        datePicker            : ".d__picker",
        btnAgregar            : ".btn__normal--agregar.btn--is-conocimientos",
        btnGuardar            : ".btn__normal--guardar.btn--is-conocimientos",
        btnCancelar           : ".btn__normal--cancelar.btn--is-conocimientos",
        btnEliminar           : ".btn__eliminar.btn--is-conocimientos",
        inputs                : "input[type='text'].CON",
        conAutocomplete       : ".autocomplete--conocimiento.CON",
        areaConocimiento      : "select[name*='pNombreDetalleCV'].CON",
        nomConocimiento       : "input[name*='pDescripcion'].CON",
        nivelConocimiento     : "select[name*='pCondicion'].CON",
        contentConocimiento   : "#wizard__conocimiento--mustache",
        tmpConocimiento       : "#tmp__conocimiento--mustache",
        listaConocimiento     : "#wizard__lista_conocimiento--mustache",
        tmpListaConocimiento  : "#tmp__lista_conocimiento--mustache",
        nuevoConocimiento     : ".wizard__lista_conocimiento--nuevo",
        step                  : "input[name='eTipo'].CON",
        message               : ".message--is-conocimiento",
        btnNext               : ".btn__next",
        stepClass             : ".CON"
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.btnAgregar              = $(st.btnAgregar)
        dom.frmRegistro             = $(st.frmRegistro)
        dom.contentConocimiento     = $(st.contentConocimiento)
        dom.listaConocimiento       = $(st.listaConocimiento)
        dom.tmpConocimiento         = $(st.tmpConocimiento)
        dom.tmpListaConocimiento    = $(st.tmpListaConocimiento)
        dom.step                    = $(st.step)
        dom.btnNext                 = $(st.btnNext)
    };
    afterCathDom = function(){};
    suscribeEvents = function (){
        dom.btnAgregar.on('click', events.agregarNuevoConocimiento)
        dom.frmRegistro.on('click', st.btnGuardar, events.guardarNuevoConocimiento)
        dom.frmRegistro.on('click', st.btnCancelar, events.cancelarNuevoConocimiento)
        dom.frmRegistro.on('click', st.btnEliminar, events.eliminarNuevoConocimiento)
    };
    events = {
        eliminarNuevoConocimiento: function () {
            step      = Sb.trigger('detectHash')
            cod = $(this).data('cod')
            Sb.trigger('deleteElementStorage', [step.hash, cod])
            fn.mostrarConocimientosGuardados()
        },
        agregarNuevoConocimiento: function () {
            dom.btnAgregar.hide()
            if(dom.frmRegistro.valid() === true){
                var step, counter, data;
                step = Sb.trigger('detectHash')
                cod = Sb.trigger('generatorID')
                data = {
                    eTipo           : dom.step.val(),
                    id              : cod.id
                }
                template = dom.tmpConocimiento.html()
                info     =  Mustache.render(template, data)
                dom.contentConocimiento.html(info)
                
                fn.cargarAreaConocimientos()
                fn.cargarConocimientos()
                fn.cargarNivelConocimientos()
                Sb.trigger('addRulesElements', [ $(st.stepClass) ])
            }else{
                // alert("No PASA")
            }
        },
        cancelarNuevoConocimiento: function () {
            dom.btnAgregar.show()
            dom.frmRegistro.data('validator').resetForm();
            dom.frmRegistro.find($(st.formField)).removeClass('error')
            dom.contentConocimiento.empty()
        },
        guardarNuevoConocimiento: function () {
            if(dom.frmRegistro.valid() === true){
                var step, counter, data;
                step = Sb.trigger('detectHash')
                cod = Sb.trigger('generatorID')

                data = {
                    eTipo             : dom.step.val(),
                    id                : cod.id,
                    cod               : dom.step.val()+'-'+cod.id,
                    pNombreDetalleCV  : $(st.areaConocimiento).val(),
                    pDescripcion      : $(st.nomConocimiento).val(),
                    pCondicion        : $(st.nivelConocimiento).val()
                }
                Sb.trigger('editDataSessionStorage2' , [step.hash , data])
                fn.mostrarConocimientosGuardados()
                dom.contentConocimiento.empty()
                dom.btnAgregar.show()
            }else{
            }
        }
    };
    fn = {
        mostrarConocimientosGuardados: function () {
            dom.listaConocimiento.empty()
            step      = Sb.trigger('detectHash')
            data      = Sb.trigger('getSessionStorage', [step.hash])
            template  = dom.tmpListaConocimiento.html()
            info      =  Mustache.render(template, data)
            dom.listaConocimiento.append(info)

            if($(st.nuevoConocimiento).length > 0)
                $(st.message).hide().closest('article').find(dom.btnNext).prop("disabled", false)
            else
                $(st.message).show().closest('article').find(dom.btnNext).prop("disabled", true)
        },
        cargarAreaConocimientos : function () {
            step      = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    $(st.areaConocimiento).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash , $(st.areaConocimiento) , ws.areaConocimiento , success])
        },
        cargarConocimientos: function () {
            success = function(oResult) {
                var _conocimientos = new autoComplete({
                    selector: st.conAutocomplete,
                    minChars: 2,
                    source: function(term, suggest){
                        term = term.toLowerCase();                        
                        var suggestions = [];
                        for (i=0 ; i<oResult.data.length ; i++){
                            if (~oResult.data[i].nombre.toLowerCase().indexOf(term)) suggestions.push(oResult.data[i].nombre);
                        }
                        suggest(suggestions);
                    }
                });
            };
            error = function(e) {
                log('cargarConocimientos', e);
            };
            Sb.trigger('createServices' , [ws.nomConocimiento, null, null , success, null, error])
        },
        cargarNivelConocimientos : function () {
            step      = Sb.trigger('detectHash')
            success = function(oResult) {
                $.each(oResult.data, function (i,k) {
                    var html = '<option value="' + k.nombre + '">' + k.nombre + '</option>';
                    $(st.nivelConocimiento).append(html);
                });
            };
            Sb.trigger('loadDataStoreSelect', [step.hash , $(st.nivelConocimiento) , ws.nivelConocimiento , success])
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['mostrarConocimientosGuardados'], fn.mostrarConocimientosGuardados, this)
    };
    return {
        init: initialize
    };
},[
    PNSU.vendorsPath + "mustache.js/mustache.min.js",
    PNSU.vendorsPath + "javascript-auto-complete/auto-complete.min.js"
]);

/**
 * PASO 5
 * @main www/js/modulos/postulante
 * @author mfyance
 */
yOSON.AppCore.addModule("load_datafields_step_six", function(Sb) {
    var events, suscribeEvents, afterCathDom, beforeCathDom, catchDom, dom, initialize, st, ws, fn;
    dom = {};
    ws = {
        directoryFiles : "./php/upload.php",
        removeFiles    : "./php/remove_file.php"
    };
    st = {
        frmRegistro           : "#form--registro--postulante",
        message               : ".message--is-documentacion",
        btnSend               : ".btn__submit",
        uploadCV              : "#wizard__cv",
        uploadDocs            : "#wizard__docs",
        docsArray             : "#wizard__docs--array",
        msj:{
            ico   : "<i class='material-icons'>pan_tool</i>&nbsp;",
            vacio : "Es necesario adjuntar los documentos solicitados",
            error : "Hay un error de subida. Inténtalo una ves más"
        }
    };
    beforeCathDom = function(){};
    catchDom = function(){
        dom.frmRegistro       = $(st.frmRegistro)
        dom.uploadCV          = $(st.uploadCV)
        dom.uploadDocs        = $(st.uploadDocs)
        dom.message           = $(st.message)
        dom.btnSend           = $(st.btnSend)
        dom.docsArray         = $(st.docsArray)
    };
    afterCathDom = function(){};
    suscribeEvents = function (){
        dom.btnSend.on("click", events.verifyAtachement)
    };
    events = {
        verifyAtachement: function (e) {
            if( dom.uploadCV.val() != "" && dom.uploadDocs.val() != "" ){
                fn.ocultarMsjError()
                return true
            }else{
                fn.mostrarMsjError(st.msj.ico , st.msj.vacio)
                return false
            }
        }
    };
    fn = {
        mostrarMsjError : function (ico, msj) {
            dom.message.find("p").append(ico + msj).end().fadeIn()
        },
        ocultarMsjError : function (msj) {
            dom.message.find("p").empty().end().hide()
        },
        cargarArchivos : function () {
            dom.uploadCV.filer({
                limit: 1,
                maxSize: 3,
                extensions: ['jpg', 'jpeg', 'pdf'],
                showThumbs: true,
                uploadFile: {
                    url: st.directoryFiles,
                    data: null,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    beforeSend: function(){},
                    success: function(data, el){
                        log("CV - data",data)
                        log("CV - el",el)
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                            $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Subido</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    },
                    error: function(el){
                        log("CV - el",el)
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                            $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");    
                        });
                    },
                    statusCode: null,
                    onProgress: null,
                    onComplete: null
                },                
                onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                    var file = file.name;
                    $.post(st.removeFiles , {file: file});
                },
                onSelect: function () {
                    fn.ocultarMsjError()
                },
                captions: {
                    button: "Elegir archivos",
                    feedback: "Elegir archivos para subir",
                    feedback2: "files were chosen",
                    drop: "Arrastre archivos aquí, para subir",
                    removeConfirmation: "Seguro de eliminar archivo?",
                    errors: {
                        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                        filesType: "Only Images are allowed to be uploaded.",
                        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                    }
                }
            });

            dom.uploadDocs.filer({
                limit: 5,
                maxSize: 3,
                extensions: ['jpg', 'jpeg', 'pdf'],
                showThumbs: true,
                changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Arrastre los archivos</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
                showThumbs: true,
                theme: "dragdropbox",
                templates: {
                    box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                    item: '<li class="jFiler-item">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-info">\
                                                <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                                <span class="jFiler-item-others">{{fi-size2}}</span>\
                                            </div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li>{{fi-progressBar}}</li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                    itemAppend: '<li class="jFiler-item">\
                                    <div class="jFiler-item-container">\
                                        <div class="jFiler-item-inner">\
                                            <div class="jFiler-item-thumb">\
                                                <div class="jFiler-item-status"></div>\
                                                <div class="jFiler-item-info">\
                                                    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                                    <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                </div>\
                                                {{fi-image}}\
                                            </div>\
                                            <div class="jFiler-item-assets jFiler-row">\
                                                <ul class="list-inline pull-left">\
                                                    <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                                </ul>\
                                                <ul class="list-inline pull-right">\
                                                    <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                </ul>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </li>',
                    progressBar: '<div class="bar"></div>',
                    itemAppendToEnd: false,
                    removeConfirmation: true,
                    _selectors: {
                        list: '.jFiler-items-list',
                        item: '.jFiler-item',
                        progressBar: '.bar',
                        remove: '.jFiler-item-trash-action'
                    }
                },
                dragDrop: {
                    dragEnter: null,
                    dragLeave: null,
                    drop: null,
                },
                uploadFile: {
                    url: "./php/upload.php",
                    data: null,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    beforeSend: function(){},
                    success: function(data, el){
                        log("Docs - data",data)
                        log("Docs - el",el)
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                            $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    },
                    error: function(el){
                        log("Docs - el",el)
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                            $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");    
                        });
                    },
                    statusCode: null,
                    onProgress: null,
                    onComplete: null
                },
                onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                    var file = file.name;
                    $.post('./php/remove_file.php', {file: file});
                },
                onSelect: function () {
                    fn.ocultarMsjError()
                },
                captions: {
                    button: "Elegir archivos",
                    feedback: "Elegir archivos para subir",
                    feedback2: "files were chosen",
                    drop: "Arrastre archivos aquí, para subir",
                    removeConfirmation: "Seguro de eliminar archivo?",
                    errors: {
                        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                        filesType: "Only Images are allowed to be uploaded.",
                        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                    }
                }
            });
        }
    };
    initialize = function(oP) {
        $.extend(st, oP);
        beforeCathDom();
        catchDom();
        afterCathDom();
        suscribeEvents();
        Sb.events(['cargarArchivos'], fn.cargarArchivos, this)
    };
    return {
        init: initialize
    };
},[
    PNSU.vendorsPath + "jquery.filer/js/jquery.filer.min.js"
]);