/**
 * Rutas para la compilación Gulp
 * @class paths.js
 * @main _frontend
 * @author mfyance
 */
var Path = {base : {}, config : {}, content : {} , scripts:{}, dest : {}, files : {}};

/**
 * Ruta base del proyecto
 * @property base
 * @type String
 */
Path.base                         = __dirname +'/../';

/**
 * Ruta base de la carpeta frontend
 * @property base
 * @type String
*/
Path.content.webconfig          = Path.base + 'Web.config';
Path.content.base               = Path.base + 'www/';
Path.content.css                = Path.content.base + 'css/';
Path.content.font               = Path.content.base + 'fonts/';
Path.content.img                = Path.content.base + 'img/';
Path.content.js                 = Path.content.base + 'js/';
Path.content.libs            	= Path.content.js 	+ 'libs/';

Path.content.jsAll             	= Path.content.js  	+ 'all/';
Path.content.jsModulos          = Path.content.js 	+ 'modulos/';

Path.content.cssAll             = Path.content.css  + 'all/';
Path.content.cssModulos         = Path.content.css 	+ 'modulos/';

Path.dest.vendors            	= Path.content.base + 'vendors/';
Path.dest.jsMin          		= Path.content.base + 'js/min/';

Path.content.cssAllMin     		= Path.content.cssAll  + '*.min.*.css';
Path.content.cssAllPath      	= [
									Path.content.cssAll  + '*.css',
									'!' + Path.content.cssAllMin
								]

Path.content.cssModulosMin 		= Path.content.cssModulos  + '**/*.min.*.css';
Path.content.cssModuloPath      = [
									Path.content.cssModulos  + '**/*.css',
									'!' + Path.content.cssModulosMin
								]

Path.content.jsAllMin     		= Path.content.jsAll  + '*.min.*.js';
Path.content.jsVendorsMin       = [
									Path.content.jsAll  + 'vendors.min.js',
									Path.content.jsAll  + 'vendors.min.*.js'
								]

Path.content.jsAllPath      	= [
									Path.content.jsAll  + '*.js',
									'!' + Path.content.jsAllMin
								]

Path.content.jsModulosMin 		= Path.content.jsModulos  + '**/*.min.*.js';
Path.content.jsModuloPath      	= [
									Path.content.jsModulos  + '**/*.js',
									'!' + Path.content.jsModulosMin
								]

Path.files.jsVendorsMain      	= [
									Path.dest.vendors  + '/jquery/dist/jquery.min.js',
									Path.dest.vendors  + '/headjs/dist/1.0.0/head.min.js',
									Path.dest.vendors  + '/jquery-ui/jquery-ui.min.js',
									Path.dest.vendors  + '/jquery.easing/js/jquery.easing.min.js',
									Path.dest.vendors  + '/bootstrap/dist/js/bootstrap.min.js',
									Path.dest.vendors  + '/admin-lte/dist/js/app.min.js',
									Path.dest.vendors  + '/jquery.cookie/jquery.cookie.js',
									Path.dest.vendors  + '/moment/min/moment.min.js',
									Path.dest.vendors  + '/moment/locale/es.js',
									Path.dest.vendors  + '/moment-timezone/builds/moment-timezone.min.js',
									Path.content.libs  + '/yoson/yoson.js'
								]

Path.files.jsExtrasMin      	= [
									Path.content.jsAll    + 'config.js',
									Path.content.jsAll    + 'main.js',
									Path.content.jsAll    + 'pnsu.js',	
									Path.content.libs  	  + 'yoson/modules.js',
									Path.content.libs  	  + 'yoson/load.js'
								]
module.exports = Path;