/**
 * Gulpfile
 * @class gulpfile.js
 * @main _frontend
 * @author mfyance
 */
var global = {}
var gulp 		  = require('gulp'),
	chalk 		  = require('chalk'),
	minifyCss 	  = require('gulp-minify-css'),
	concatCss 	  = require('gulp-concat-css'),
	concatJs 	  = require('gulp-concat'),
	rename 		  = require('gulp-rename'),
	rimraf 		  = require('gulp-rimraf'),
	browserSync   = require('browser-sync'),
	nodemon 	  = require('gulp-nodemon'),
	runSequence   = require('run-sequence'),
	uglify 		  = require('gulp-uglify'),
	git 		  = require('git-rev-sync'),
	bower 		  = require('gulp-bower'),
	preen 		  = require('preen'),
	config 		  = require('./config'),
	ports 		  = require('./ports'),
	path    	  = require('./paths');



/**
 * Gulp App
 * @task {gulp esti} Crear css y js [concat + minified + versioned]
 */
gulp.task('app', ['env', 'version'] , function (done) {
	if(global.environment === "LOCAL"){
		runSequence('clean');
	}
	else{
		runSequence('js','css','version:change','bower:filter');
	}
	done();
})


/**
 * Gulp Bower
 * @task {gulp bower} Inicia la instalación
 * @task {gulp bower:install} Inicia la descarga
 * @task {gulp bower:filter} Elimina archivos innecesarios que descargo el bower, despues de terminar la tarea de instalaciòn
 */
gulp.task('bower' , function(done){
	runSequence('clean:vendors', 'bower:install', function(){
		console.log(
			chalk.black.bgGreen('Bower:') +
			chalk.white.bgBlack.bold(':') +
			chalk.white.bgCyan.bold('Instalados correctamente')
		);
	});
	done();
});
gulp.task('bower:filter', function(cb) {
 	preen.preen({}, cb);
});
gulp.task('bower:install', function() {
  	return bower()
	.pipe(gulp.dest(path.dest.vendors))
});

/**
 * Gulp Env 
 * @task {gulp env} Muestra el entorno de trabajo
 */
gulp.task('env', function(){
	global.environment = config.environment()
	console.log(
		chalk.black.bgGreen('Environment:') +
		chalk.white.bgBlack.bold(':') +
		chalk.white.bgCyan.bold(global.environment)
	);
	return global.environment
})

/**
 * Gulp Version
 * @task {gulp version} Obtiene la versión abreviada del commit
 */
gulp.task('version', function(){
	global.version = git.short()
	console.log(
		chalk.black.bgGreen('Version Statics:') +
		chalk.white.bgBlack.bold(':') +
		chalk.white.bgCyan.bold(global.version)
	);
})

/**
 * Gulp exportar el global.version
 * @task {gulp version:global} Obtiene la versión abreviada del commit
 * @returns {global.version} Retorna el parametro global.version
 */
gulp.task('version:global', function(){
	global.version = git.short()
	return global.version
})

/**
 * Gulp para editar la version de lso estaticos en el Web.Config
 * @task {gulp version:change} Obtiene la versión abreviada del commit
 */
gulp.task('version:change', ['version:global'], function(){
	config.version(global.version)
	console.log(
		chalk.black.bgGreen('Web.Config {staticVersion}') +
		chalk.white.bgBlue(' <Editado> ') +
		chalk.white.bgBlack.bold(':') +
		chalk.blue.bgCyan.bold(global.version)
	);
})

/**
 * Gulp Js
 * @task {gulp js} Limpia los archivos generados dinámicamente y los vuelve a generar
 * @task {gulp js:vendors:main} Genera un archivo vendors.js s/ version
 * @task {gulp js:modules} Concatena + Versiona + Minifica a TODO (excepto ALL) los módulos
 * @task {gulp js:this} Concatena + Versiona + Minifica determinado módulo. 
 * Ejm: gulp js:this -thismodule/module.js 
 */
gulp.task('js', ['clean:js'] , function (done) {
	runSequence('js:modules', 'js:vendors:main','js:extras', function () {
		console.log(
			chalk.black.bgGreen('Archivos JS generados') +
			chalk.white.bgBlack.bold(':') +
			chalk.white.bgBlue(' Principales Js & Módulos ')
		);
	});
	done();
});

gulp.task('js:vendors:main', ['version:global'], function (done) {
	return gulp.src(path.files.jsVendorsMain)
		.pipe(concatJs('vendors.min' + global.version + '.js'))
		.pipe(uglify())
		.pipe(gulp.dest(path.dest.jsMin))
});

gulp.task('js:extras', ['version:global'], function(){
	return gulp.src(path.files.jsExtrasMin)
		.pipe(rename({ suffix: '.min.' + global.version }))
		.pipe(uglify())
		.pipe(gulp.dest(path.dest.jsMin))
})

gulp.task('js:modules', ['version:global'], function(){
	return gulp.src(path.content.jsModuloPath)
		.pipe(rename({ suffix: '.min.' + global.version }))
		.pipe(uglify())
		.pipe(gulp.dest(path.content.jsModulos))
})

gulp.task('js:this', ['version:global'], function() {
    _path = process.argv[3].substr(1, process.argv[3].length);
    splitPath = _path.split('/');
    module = splitPath[0]+'/';
    file = splitPath[splitPath.length-1].split('.');
    js = file[0]+'.'+file[1];
    mypath = path.content.jsModulos + module + js	
	console.log(
		chalk.black.bgGreen('Módulo JS generado') +
		chalk.white.bgBlack.bold(':') +
		chalk.white.bgBlue(module+file[0]+".min."+global.version+".js")
	);
    return gulp.src(path.content.jsModulos + module + js)
		.pipe(concatJs(file[0] + '.min.' + global.version + '.js'))
		.pipe(uglify())
		.pipe(gulp.dest(path.content.jsModulos + module))
})

/**
 * Gulp Css
 * @task {gulp css} Limpia los archivos generados dinámicamente y los vuelve a generar
 * @task {gulp css:all} Concatena + Versiona + Minifica a los archivos dentro de ALL
 * @task {gulp css:module} Concatena + Versiona + Minifica a TODO (excepto ALL) los módulos
 * @task {gulp css:this} Concatena + Versiona + Minifica determinado módulo. 
 * Ejm: gulp css:this -thismodule/module.css 
 */
gulp.task('css',  ['clean:css'] , function (done) {
	runSequence('css:all', 'css:module', function () {
		console.log(
			chalk.black.bgGreen('Archivos CSS generados') +
			chalk.white.bgBlack.bold(':') +
			chalk.white.bgBlue(' Principales Css & Módulos ')
		);
	});
	done();	
})
gulp.task('css:all', ['version:global'], function() {
	return gulp.src(path.content.cssAllPath)
		.pipe(minifyCss({compatibility: 'ie8'}))
		.pipe(rename({ suffix: '.min.' + global.version }))
		.pipe(gulp.dest(path.content.cssAll))
})
gulp.task('css:module', ['version:global'], function() {
	return gulp.src(path.content.cssModuloPath)		
		.pipe(minifyCss({compatibility: 'ie8'}))
		.pipe(rename({ suffix: '.min.' + global.version }))
		.pipe(gulp.dest(path.content.cssModulos))
})
gulp.task('css:this', ['version:global'], function() {
    _path = process.argv[3].substr(1, process.argv[3].length);
    splitPath = _path.split('/');
    module = splitPath[0]+'/';
    file = splitPath[splitPath.length-1].split('.');
    css = file[0]+'.'+file[1];
    mypath = path.content.cssModulos + module + css	
	console.log(
		chalk.black.bgGreen('Módulo CSS generado') +
		chalk.white.bgBlack.bold(':') +
		chalk.white.bgBlue(module+file[0]+".min."+global.version+".js")
	);
    return gulp.src(path.content.cssModulos + module + css)
	.pipe(concatCss(file[0] + '.min.' + global.version + '.css'))
	.pipe(minifyCss({compatibility: 'ie8'}))
	.pipe(gulp.dest(path.content.cssModulos + module))
})

/**
 * Gulp Clean
 * @task {gulp clean} Elimina los css y js creados dinámicamente.
 * @task {gulp clean:css} Elimina todo los archivos css generados
 * @task {gulp clean:js} Elimina todo los archivos js generados
 * @task {gulp clean:css:all} Elimina all.min.[version].css
 * @task {gulp clean:js:all} Elimina all.min.[version].js
 * @task {gulp clean:css:modules} Elimina [modulo].min.[version].css
 * @task {gulp clean:js:modules} Elimina [modulo].min.[version].js
 * @task {gulp clean:js:vendors:main} 
 */
gulp.task('clean', function (cb) {
	runSequence('clean:js', 'clean:css', cb)
})
gulp.task('clean:css', function (done) {
	runSequence('clean:css:all', 'clean:css:modules', function(){
		console.log(
			chalk.black.bgGreen('All & Modules') +
			chalk.white.bgBlue(' CSS ') +
			chalk.white.bgBlack.bold(':') +
			chalk.white.bgCyan.bold('Eliminados correctamente')
		);
	});
	done();
})
gulp.task('clean:css:all', function () {
	return gulp.src(path.content.cssAllMin, { read: false })
	.pipe(rimraf({ force: true }))
})
gulp.task('clean:css:modules', function () {
	return gulp.src(path.content.cssModulosMin, { read: false })
	.pipe(rimraf({ force: true }))
})
gulp.task('clean:js', function (done) {
	runSequence('clean:js:min', 'clean:js:modules','clean:js:modules', function(){
		console.log(
			chalk.black.bgGreen('All & Modules & MainVendors') +
			chalk.white.bgBlue(' <minified>') +
			chalk.white.bgBlack.bold(':') +
			chalk.white.bgCyan.bold('Eliminados correctamente')
		);
	});
	done();
})
gulp.task('clean:js:min', function () {
	return gulp.src(path.dest.jsMin, { read: false })
	.pipe(rimraf({ force: true }))
})
gulp.task('clean:js:modules', function () {
	return gulp.src(path.content.jsModulosMin, { read: false })
	.pipe(rimraf({ force: true }))
})
gulp.task('clean:js:vendors:main', function () {
	return gulp.src(path.dest.jsMin + 'vendors.*.*', { read: false })
	.pipe(rimraf({ force: true }))
})

/**
 * Gulp Vendors
 * @task {gulp clean:vendors} Elimina la carpeta vendors
 */
gulp.task('clean:vendors', function () {
	return gulp.src(path.dest.vendors, { read: false })
	.pipe(rimraf({ force: true }))
})

/**
 * Tarea para ejecutar el browserSync, una vez ejecutada la tarea nodemon
 * @task {gulp server} Inicia el servidor local
 */
gulp.task('server', ['nodemon'], function () {
	browserSync({
		proxy: "http://localhost:" + ports.server,
		files: ["../**/*.*"],	
        port: ports.server
	})
})

/**
 * Tarea para ejecutar el servidor express mediante el script server.js
 * @task {gulp nodemon}
 */
gulp.task('nodemon', function (cb) {
	var called = false;
	return nodemon({
		script: 'server.js'
	})
	.on('start', function () {
		if (!called) {
			called = true;
			cb();
		}
	})
	.on('restart', function () {
		setTimeout(function () {
			browserSync.reload({
				stream: false
			});
		}, 1000)
	})
	.on('crash', function(){
		console.log("Error en la tarea: gulp watch'")
	});
})
