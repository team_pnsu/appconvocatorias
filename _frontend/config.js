/**
 * Funciones para leer y editar el Web.Config (ASPX)
 * @class config.js
 * @main _frontend
 * @author mfyance
 */
var fs 				= require('fs'),
	DOMParser 		= require('xmldom').DOMParser,
	path    		= require('./paths');

path.environment 	= "enviroment"
path.staticVersion 	= "staticVersion"

module.exports = { 
    environment : function (cb) {
    	var data, xml, add;
    	data = fs.readFileSync(
    		path.content.webconfig,
    		'utf8', 
    		function(error, data){
		  		if (err) return console.log(err);
			}
		);
		xml = new DOMParser().parseFromString(data)  
		add = xml.documentElement.getElementsByTagName('add');

		for(i = 0 ; i < add.length; i++){
			if (add[i].getAttribute("key") === path.environment) { 
		  		return add[i].getAttribute("value")
			}
		}
    },
    version : function (version,cb) {
    	var data, xml, add, oldVersion, newVersion;
		data = fs.readFileSync(
			path.content.webconfig,
			'utf8', 
			function(err, data) {
			    if (err) return console.log(err);
			}
		);
		xml = new DOMParser().parseFromString(data)  
		add = xml.documentElement.getElementsByTagName('add');

		for(i = 0 ; i < add.length; i++){
			if (add[i].getAttribute("key") === path.staticVersion) { 
		  		oldVersion = add[i].getAttribute("value")
		  		newVersion = data.replace(oldVersion,version);
			}
		}
	    fs.writeFile(
	    	path.content.webconfig,
	    	newVersion,
	    	'utf8',
	    	function(err) {
	        	if (err) return console.log(err);
	    	}
	    );
    }
}