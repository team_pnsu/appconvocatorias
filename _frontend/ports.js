var Ports = {};
Ports = {
	server :  5000,
	services : 4444
}
if(typeof module != 'undefined'){
	module.exports = Ports;
}else{
	window.SERVER = Ports.server;
	window.SERVICES = Ports.services;
}